#version 120

uniform sampler2D tex;

varying vec2 uv;
uniform vec4 color_add = vec4(0,0,0,0);

void main(void) {
  vec4 col = texture2D(tex, uv);
  col = col + color_add;
  gl_FragColor = col;
  
}