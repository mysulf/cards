#version 120

uniform float width;
uniform float height;
attribute vec2 coord2d;

uniform vec2 pos;

mat4 mod = mat4(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1);
mat4 transl = mat4(
1,0,0,0,
0,1,0,0,
0,0,1,0,
10,10,0,1);
mat4 scale = mat4(
1,0,0,0,
0,1,0,0,
0,0,1,0,
0,0,0,1);
mat4 view = mat4(
1,0,0,0,
0,1,0,0,
0,0,1,0,
0,0,0,1);

uniform mat4 proj;


void main(void) {
scale = mat4(
width,0,0,0,
0,height,0,0,
0,0,1,0,
0,0,0,1);
  mod[3].x = pos.x;
  mod[3].y = pos.y;
  gl_Position = (proj * mod * scale) * vec4(coord2d.x, coord2d.y, 0, 1.0) ;

}