#version 120

uniform sampler2D tex;
uniform vec4 color = vec4(0,0,0,1);
varying vec2 uv;

void main(void) {
  vec4 col = texture2D(tex, uv);
  col *= color;
  //if(col.a == 0)
	//col = vec4(1,0,0,1);
  gl_FragColor = col;
  
}