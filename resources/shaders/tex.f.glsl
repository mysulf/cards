#version 120
uniform sampler2D tex;
uniform float opacity = 1.0;
uniform vec4 color_add = vec4(0,0,0,0);
void main() {
    vec4 col = texture2D(tex, gl_TexCoord[0].st);
    col = col + color_add;
	
	if(col.a > 0){
		col.a = col.a * opacity;
		
	}
	else{
	col.a = 0;
	}
    gl_FragColor = col;
}