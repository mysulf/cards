#version 120

attribute vec2 coord2d;
attribute vec2 texcoord;

varying vec2 uv;

uniform vec2 pos;

mat4 mod = mat4(1,0,0,0,
				0,1,0,0,
				0,0,1,0,
				0,0,0,1);

mat4 transl = mat4(1,0,0,0,
				0,1,0,0,
				0,0,1,0,
				10,10,0,1);

mat4 scale = mat4(1,0,0,0,
				0,1,0,0,
				0,0,1,0,
				0,0,0,1);

mat4 view = mat4(1,0,0,0,
				0,1,0,0,
				0,0,1,0,
				0,0,0,1);


uniform mat4 proj;


void main(void) {
  mod[3].x = pos.x;
  mod[3].y = pos.y;
  gl_Position = (proj * view * (mod * scale)) * vec4(coord2d.xy, 0.0, 1.0) ;
  uv = texcoord;
}