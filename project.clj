(defproject cards "0.1.0-SNAPSHOT"
  :description "A fantasy card game"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [
                  [org.clojure/clojure "1.5.1"]
                  [org.lwjgl.lwjgl/lwjgl "2.9.0"]
                  [org.lwjgl.lwjgl/lwjgl_util "2.9.0"]
                  [org.lwjgl.lwjgl/lwjgl-platform "2.9.0"
                   :classifier "natives-windows"
                   ;; LWJGL stores natives in the root of the jar; this
                   ;; :native-prefix will extract them.
                   :native-prefix ""]
                  ]
  )