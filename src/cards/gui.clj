(ns cards.gui
  ;(:require [cards.engine :refer [width height draw-quad draw-string GL-select t glob-sprites glob-textures glob-fonts draw-tex push-]])
  (:use [cards engine])
  )



(def LOG-POSY 150)
(def LOG-HEIGHT (- height 330))

(defn draw-gui[game]
  (loop [queue (reduce #(conj %1 ((:entities (:gui-state game)) %2)) [] (:render-targets (:gui-state game)))
         ]
    (when (> (count queue) 0)
      (let [it (first queue)]
        (do
          ((:render it) game it)
          (when-not (or (nil? (:mouse-info it)) (not (:mouse-over it)))
            ((:mouse-info it) it)
            )
          )
        (recur (flatten (conj (vec (map (:entities (:gui-state game)) (:children it))) (rest queue))))))))

(defn update-gui-item [game item]
  (assoc-in game [:gui-state :entities (:id item)] item))

(defn show[gui-state item]
  (assoc gui-state :render-targets  (vec (distinct (conj (:render-targets gui-state) (:id item))))))

(defn hide[gui-state item]
  (assoc gui-state :render-targets   (vec (distinct  (filter #(not= (:id item) %) (:render-targets gui-state))))))

(defn disable[gui-state item]
  (assoc-in gui-state [:entities (:id item)] (assoc item :enabled false)))

(defn enable [gui-state item]
  (assoc-in gui-state [:entities (:id item)] (assoc item :enabled true)))

(defn draw-gui-state[game interpolation]
  (draw-gui game))

(defn add-child[gui-item child-id]
  (assoc gui-item :children  (conj (:children gui-item) child-id)))

(defn add-gui-item [state item]
  (assoc state :entities (assoc (:entities state) (:id item) item)))

(defn create-gui-item[x y w h render]
  {:id (generate-id)
   :children ()
   :render render
   :posx x :posy y :width w :height h
   }
  )

;(defn create-panel[x y w h]
;  (create-gui-item x y w h draw-panel))

(defn draw-button[game btn]
  (let [x (:posx btn)
        y (:posy btn)
        w (:width btn)
        h (:height btn)
        pt (:pressed-tex btn)
        ut (:unpressed-tex btn)]
    (if (:enabled btn)
      (if (:pressed btn)
        (draw-tex (assoc (:sprite btn) :id (:id btn) :width w :height h :posx x :posy y) pt)
        (draw-tex (assoc (:sprite btn) :id (:id btn) :width w :height h :posx x :posy y) ut)
        )
      (draw-tex (assoc (:sprite btn) :id (:id btn) :width w :height h :posx x :posy y) ut :color-add [-0.15 -0.15 -0.15 0.0])
      )
;    (draw-string (:mine-8 @glob-fonts) text (+ x 3)  (+ y 1))
    ))



;; remove a gui item and all its' children from :entities and :render-targets
(defn remove-gui-item [game item-id]
  (let [game (reduce #(assoc-in %1 [:gui-state :entities] (dissoc (:entities (:gui-state game)) %2))
               game (:children ((:entities (:gui-state game)) item-id)))
        game (assoc-in game [:gui-state :entities] (dissoc (:entities (:gui-state game)) item-id))]
    (assoc-in game [:gui-state :render-targets] (set (filter #(not= % item-id) (:render-targets (:gui-state game)))))
    )
  )

(defn create-button[x y w h pt ut sprite]
  (assoc (create-gui-item x y w h draw-button) :pressed-tex pt :unpressed-tex ut :sprite sprite :enabled true))

(defn create-default-button[x y w h pt ut & {:keys [sprite] :or {sprite (:btn @glob-sprites)}}]
  (assoc (create-button x y w h pt ut sprite)
    :on-mouse-enter (fn [game item] (update-gui-item game (assoc item :mouse-over true)))
    :on-mouse-leave (fn [game item] (update-gui-item game (assoc item :pressed false :mouse-over false)))
    :on-mouse-down  (fn [game item] (update-gui-item game (if (:enabled item) (assoc item :pressed true) item)))
    :on-mouse-up (fn [game item]
                   (let [game (update-gui-item game (assoc item :pressed false))
                         state (if (:pressed item) ((:on-click item) game item) game)]
                     (if (nil? state)
                       game
                       state)))))

(defn draw-log[game log]
  (doseq [l (range (count (:log game)))]
;    (when (< (:posy log) LOG-HEIGHT)
      (draw-static-string (nth (:log game) l) (:posx log) (+ (:posy log) (* l 20)))
;      )

    )
  )

(defn get-dimensions [string & {:keys [font] :or {font (:celtic-12 @glob-fonts)}}]
  (let [w (get-string-width font string)
        h (get-string-height font string)]
    {:w w :h h}
    )
  )

(defn draw-skill-info[item]
  (let [skill (:skill item)
        skill-content-keys (sort (keys (-> skill (dissoc :hero-id) (dissoc :id) (dissoc :name) (dissoc :type) (dissoc :key))))
        skill-string (str (:name skill) "\n"
                       (if (not (:active skill)) "Passive\n" "")
                       (:text skill) "\n"
                       (if (not= (:cooldown skill) 0)
                         (str "Cooldown: " (:cooldown-counter skill) " (" (:cooldown skill) ")")
                         ""
                         )
                       (if (not= (:duration skill) 0)
                         (str "Duration: " (:duration skill))
                         ""
                         )
                       (if (not= (:stamina-cost skill) 0)
                         (str "Stamina: " (:stamina-cost skill))
                         ""
                         )
                       )
        dimensions (get-dimensions skill-string)
;        (reduce (fn [dim k]
;                     (let [s (str k " : " (k skill))
;                           w (. (:ac (:celtic-12 @glob-fonts)) getWidth s)
;                           n (assoc dim :h (+ (:h dim) 17))
;                           ]
;                       (if (> w (:w n)) (assoc n :w w) n)
;                       )
;
;                     ) {:w 0 :h 0} skill-content-keys)
        h (+ 0 (:h dimensions))
        x (+ 3 (:posx item))
        y (- (:posy item) (+ h 10))
        w (+ (:w dimensions) 5)]

    (draw-quad x y w h :r 0.9 :g 0.9 :b 0.8 :a 1.0)

    (draw-string (:celtic-12 @glob-fonts) skill-string (+ x 2) (+ y 2))
;    (doseq[s (range (count  skill-content-keys))]
;      (draw-string (:celtic-12 @glob-fonts) (str (nth skill-content-keys s) " : "  ((nth skill-content-keys s) skill)) (+ x 2) (+ y 17 (* s 17)))
;      )

    )


  )
(defn draw-popup-info [game item]
  (draw-quad (:posx item) (:posy item) (:width item) (:height item) :r 0.9 :g 0.9 :b 0.8 :a 1.0)
  (let [strings (clojure.string/split-lines (:content item))]
    (doseq [s (range (count strings))]
      (draw-string (:celtic-12 @glob-fonts) (nth strings s) (+ (:posx item) 2) (+ (:posy item) 2 (* s 17)))
      )
    )
  )



(defn create-gui[game]
  (let [gui-state {:entities {} :render-targets []}
        path-btn (create-default-button 915 500 100 100 (:advance-btn2 @glob-textures) (:advance-btn1 @glob-textures))
        path-btn (assoc path-btn :on-click (fn [game item](assoc game :progress true)))

        skill-btn (create-default-button 0 0 25 25 (:skill-btn2 @glob-textures) (:skill-btn1 @glob-textures) :sprite (:skill-sprite @glob-sprites))
        skill-btn (assoc skill-btn :on-click (fn [game item](assoc game :skill (:skill item))))
        skill-btn (assoc skill-btn :mouse-info draw-skill-info)

        eng-btn (create-default-button 1005 500 100 100 (:engage-btn2 @glob-textures) (:engage-btn1 @glob-textures))
        eng-btn (assoc eng-btn :on-click (fn [game item](assoc game :spawn-monsters true)))

        popup-info (create-gui-item 0 0 0 0 draw-popup-info)

        log (create-gui-item 50 LOG-POSY 500 LOG-HEIGHT draw-log)
        skill-popup (create-gui-item 50 150 500 750 draw-skill-info)


       ]

    (assoc game
      :show-log (fn[game] (assoc game :gui-state (show (hide (:gui-state game) log) log)))
      :hide-log (fn[game] (assoc game :gui-state (hide (:gui-state game) log)))

      :show-skill-buttons (fn[game startx starty skills]
                            (let [buttons (for [s (range (count skills))]
                                            (let [skill (nth skills s)]
                                              (assoc skill-btn
                                                :posx (+ startx (* s 27))
                                                :posy starty
                                                :skill skill
                                                :id (:id skill)
                                                :enabled true
                                                :id (:id skill)
                                                :enabled (< (:cooldown-counter skill) 1)
                                                )
                                              )
                                            )]

                              (reduce (fn[game button]
                                        (let [game (update-gui-item game button)]
                                          (assoc game :gui-state (show (:gui-state game) button))
                                          ))
                                (assoc-in game [:gui-state :skill-buttons] buttons) buttons))
                            )

      :hide-skill-buttons (fn[game] (assoc game :gui-state
                                      (reduce #(hide (disable %1 %2) %2) (:gui-state game) (:skill-buttons (:gui-state game)))))
      :disable-skill-buttons (fn [game] (assoc game :gui-state
                                         (reduce #(disable %1 %2) (:gui-state game) (:skill-buttons (:gui-state game)))))
      :show-popup (fn [game string-content x y]
                    (let [dim (get-dimensions (str string-content))
                          info (assoc popup-info :width (+ 3 (:w dim)) :height (+ 3 (:h dim)) :posx x :posy (- y (:h dim)) :content (str string-content))
                          game (update-gui-item game info)]
                        (assoc game :gui-state (show (:gui-state game) info))
                      )
                    )

      :hide-popup (fn [game]
                    (assoc game :gui-state (hide (:gui-state game) popup-info)))

      :show-choice (fn[game] (assoc game :gui-state
                               (-> (:gui-state game)
                               (show eng-btn)
                               (show path-btn))))
      :hide-choice (fn[game] (assoc game :gui-state
                               (-> (:gui-state game)
                                 (hide eng-btn)
                                 (hide path-btn))))

      :gui-state (-> gui-state
;                             (add-gui-item popup-info)
                             (add-gui-item path-btn)
                             (add-gui-item eng-btn)
                             (add-gui-item log)

;                             (add-gui-item skill-btn)
;                             (show skill-btn)
;                             (show path-btn)

                             )
      )

    )
  )
(defn get-gui-entity [game id]
  ((:entities (:gui-state game)) id))


(defn update-mouse-listeners[game]
  (let [sel-id (:sel-id (:gui-state game))
        gui-item (get-gui-entity game sel-id)
        old-gui-item (get-gui-entity game (:old-sel-id (:gui-state game)))]

    (if (not= sel-id (:old-sel-id (:gui-state game)))

      (let [menter (:on-mouse-enter gui-item)
            mleave (:on-mouse-leave old-gui-item)
            game (if (not (nil? menter))
                   (menter game gui-item)
                   game
                   )
            game (if (not (nil? mleave))
                   (mleave game old-gui-item)
                   game
                   )
            ]
        game
        )
    game
      )
    )
  )

(defn update-gui-state[game]
  (let [sel-id (mouse-select (map (:entities (:gui-state game)) (:render-targets (:gui-state game))));(GL-select game draw-gui-state)

        game (assoc game :gui-state (assoc (:gui-state game) :old-sel-id (:sel-id (:gui-state game))))
        game (assoc game :gui-state (assoc (:gui-state game) :sel-id sel-id))
        game (update-mouse-listeners game)

        old-gui-item (get-gui-entity game (:old-sel-id (:gui-state game)))

        gui-item (get-gui-entity game sel-id)
;        game (assoc game :gui-state (assoc (:gui-state game) :sel-id sel-id))
        input-state (:input-state game)
        ]
    (cond
      (contains? (:mouse-down input-state) 0)
      (let [listener (:on-mouse-down gui-item)]
        (if (nil? listener)
          game
          (listener game gui-item)
          )
        )

      (contains? (:mouse-released input-state) 0)
      (let [listener (:on-mouse-up gui-item)]
        (if (nil? listener)
          game
          (listener game gui-item)
          )
        )

      :else
      game
      )
    )
  )
