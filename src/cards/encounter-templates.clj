
(def monsters
  {
    1
    {
      :type :monster
      :name "Goblin Hunter"
      :dmg-dice-num 1
      :dmg-dice-sides 2
      :crit-multiplier 2
      :crit 20
      :damage-type :physical
      :resistances {:physical 12 :magic 0 :fire 0 :poison 0 :thorns 0}
      :attack 2
      :hp 6
      :max-hp 6
      :bonus-dmg 0
      :initiative 7
      }

    2
    {
      :type :monster
      :name "Goblin Scout"
      :dmg-dice-num 1
      :dmg-dice-sides 2
      :crit-multiplier 2
      :crit 20
      :damage-type :physical
      :resistances {:physical 10 :magic 0 :fire 0 :poison 0 :thorns 0}
      :attack 1
      :hp 4
      :max-hp 4
      :bonus-dmg 0
      :initiative 2
      }

    3
    {
      :type :monster
      :keywords #{:goblin}
      :name "Goblin Raider"
      :dmg-dice-num 1
      :dmg-dice-sides 3
      :crit-multiplier 2
      :crit 19
      :damage-type :physical
      :resistances {:physical 12 :magic 0 :fire 0 :poison 0 :thorns 0}
      :attack 1
      :hp 5
      :max-hp 5
      :bonus-dmg 0
      :initiative 2
      }

    4
    {
      :type :event
      :sub-type :buff
      :name "Goblin Feast"
      :text "Goblins everywhere. Limbs and intestines of something unknown, foul smells
                                                   of rotten excitement."
      :on-reveal [(fn [game id]
                    (let [monsters (filter #(= (:type %) :monster) (vals (:characters game)))]
                      (if (empty? monsters)
                        game
                        (reduce #(set-character %1 (increment-stat %2 :attack 2)) game monsters)
                        )))]
      }

    5
    {
      :type :event
      :sub-type :trap
      :name "Spike Trap"
      :text "Caught! Take 1d4 damage per round for 2 rounds."
      :on-reveal [(fn[game id]
                    (let [trap-dmg {:dmg-dice-num 1
                                    :dmg-dice-sides 4
                                    :attack 0
                                    :damage-type :physical
                                    :crit 20
                                    :crit-multiplier 1
                                    :bonus-dmg 0
                                    :name "Spike Trap"}]

                      (apply-effect game
                        (assoc (make-effect :damage :spike-trap 2 trap-dmg id)
                          :target-type :hero :num-targets 1)
                        (list (rand-nth (:heroes game))))))]
      }
    6
    {
      :type :event
      :sub-type :terrain
      :name "Clearing"
      :text "You pick up speed... Gain stamina."
      :on-reveal [(fn[game id] (assoc game :stamina (+ (:stamina game) 3)))]
      }
    7
    {
      :type :event
      :sub-type :terrain
      :name "Roots and snares"
      :text "Caught in a tangle. Loose stamina."
      :on-reveal [(fn[game id] (assoc game :stamina (- (:stamina game) 1)))]
      }
    8
    {
      :type :event
      :sub-type :terrain
      :name "Thorns"
      :text "Thorns cut your skin."
      :on-reveal [(fn[game id]
                    (let [trap-dmg {:dmg-dice-num 1
                                    :dmg-dice-sides 2
                                    :attack 0
                                    :damage-type :thorns
                                    :crit 20
                                    :crit-multiplier 2
                                    :bonus-dmg 0
                                    :name "Thorns"}]
                      (apply-effect game
                        (make-effect :damage :thorns 2 trap-dmg id)
                        (pick-n-random 2 (:heroes game)))))]
      }
    9
    {
      :type :event
      :sub-type :terrain
      :name "Poisonous Mushroom"
      :text "You step on something and the last thing you remember is the foul smell."
      :on-reveal [(fn[game id]
                    (apply-effect game (make-effect :stun :poisonous-mushroom 2 {} id)
                      (list (rand-nth (:heroes game)))))]
      }
    10
    {
      :type :event
      :sub-type :ominous-things
      :name "Crows"
      :text "You are being watched."
      :on-reveal [(fn[game id]
                    (apply-effect game (make-effect :debuff :crows 2 {:resistances {:physical 2}} id)
                      (:heroes game)))]
      }
    11
    {
      :type :monster
      :name "Snake"
      :keywords #{:beast}
      :dmg-dice-num 1
      :dmg-dice-sides 2
      :crit-multiplier 2
      :crit 20
      :damage-type :physical
      :resistances {:physical 15 :magic 0 :fire 0 :poison 0 :thorns 0}
      :attack 0
      :hp 4
      :max-hp 4
      :bonus-dmg 0
      :initiative 7
      }

    12
    {
      :type :monster
      :name "Wolf"
      :keywords #{:beast}
      :dmg-dice-num 1
      :dmg-dice-sides 3
      :crit-multiplier 2
      :crit 20
      :damage-type :physical
      :resistances {:physical 12 :magic 0 :fire 0 :poison 0 :thorns 0}
      :attack 0
      :hp 5
      :max-hp 5
      :bonus-dmg 0
      :initiative 2
      }

    13
    {
      :type :monster
      :keywords #{:beast}
      :name "Boar"
      :dmg-dice-num 1
      :dmg-dice-sides 4
      :crit-multiplier 2
      :crit 19
      :damage-type :physical
      :resistances {:physical 13 :magic 0 :fire 0 :poison 0 :thorns 0}
      :attack 0
      :hp 6
      :max-hp 6
      :bonus-dmg 0
      :initiative 2
      }
    14
    {
      :type :monster
      :keywords #{:beast}
      :name "Black Bear"
      :dmg-dice-num 1
      :dmg-dice-sides 6
      :crit-multiplier 2
      :crit 20
      :damage-type :physical
      :resistances {:physical 15 :magic 0 :fire 0 :poison 0 :thorns 0}
      :attack 2
      :hp 8
      :max-hp 8
      :bonus-dmg 0
      :initiative 2
      }
    }
  )

(def encounter-sets {
                      1 {
                         :name "Goblin Menace"
                         :text "The immigrant goblins from the north, or \"forest goblins\", arrived some
                         hundred years ago. While not particularly deadly in combat, their sneaky ways have
                         lured countless of unbeknownst peasants and village people deep into the woods, ripe
                         for mugging and thieving."
                         :cards [2 2 2 1 1 1 3 3 5 4]
                         }
                      2 {
                         :name "Murky Forests"
                         :text "The dank forests around these lands are old and wise and have caused trouble
                         for the not so careful but curious mapper willing to put his name on unexplored lands.
                         Today, these forests are almost forgotten for the man, for the joy of many a foul creature,
                         and, for the forest itself."
                         :cards [6 6 6 6 7 7 8 9 10 10]
                         }
                      3 {
                         :name "Forest Beasts"
                         :text "The animals have grown restless lately, tread carefully."
                         :cards [11 11 11 11 14 13 13 12 12 12]
                         }
                      }
  )

(def encounters {
                  1 {:name "Lorian Woods"
                     :id 1
                     :level 1

                     ;; game mode determines how cards are drawn, how the encounter progresses,
                     ;; how equipment and heroes are unlocked etc.
                     :game-mode :standard

                     ;; id's pointing to encounter sets
                     :encounter [1 2 3]

                     ;; the rewards refer to item and hero id's
                     :first-time-rewards [{:hero 5}
                                          {:item 32}
                                          {:hero-enhancement :strength}]

                     ;; guaranteed 1 reward from tier 1
                     ;; subsequent tiers are rolled according to the tier probabilities
                     :rewards [{:item 31 :tier 1} {:item 2 :tier 1} {:hero-enhancement :random} {:item 45 :tier 2}]

                     :unlocked-by []
                     :artifacts-required []
                     :unlocks [2]
                     }

                  2 {:name "Lorian Plains"
                     :id 2
                     :level 1

                     ;; game mode determines how cards are drawn, how the encounter progresses,
                     ;; how equipment and heroes are unlocked etc.
                     :game-mode :standard

                     ;; id's pointing to encounter sets
                     :encounter [1 2 3]

                     ;; the rewards refer to item and hero id's
                     :first-time-rewards [{:hero 5}
                                          {:item 32}
                                          {:hero-enhancement :strength}]

                     ;; guaranteed 1 reward from tier 1
                     ;; subsequent tiers are rolled according to the tier probabilities
                     :rewards [{:item 31 :tier 1} {:item 2 :tier 1} {:hero-enhancement :random} {:item 45 :tier 2}]

                     :unlocked-by [1]
                     :artifacts-required []
                     :unlocks [3 4]
                     }
                  })
