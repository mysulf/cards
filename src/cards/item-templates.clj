
(def items
  {1 {
     :type :weapon
     :equip :1-hand
     :name "Dagger"
     :dmg-dice-num 1
     :dmg-dice-sides 4
     :crit-multiplier 2
     :crit 15
     :damage-type :physical
     :bonus-dmg 0
     :attack 1
     :tex :eq-dagger
     }
   2 {
     :type :weapon
     :equip :2-hand
     :name "Bow"
     :dmg-dice-num 1
     :dmg-dice-sides 6
     :crit-multiplier 2
     :crit 19
     :damage-type :physical
     :bonus-dmg 0
     :attack 2
     :tex :eq-bow
     }
   3 {
     :type :armour
     :equip :armour
     :name "Leather Armour"
     :resistances {:physical 2}
     :tex :eq-armour
     }
   4 {
     :type :shield
     :equip :1-hand
     :name "Shield"
     :resistances {:physical 2}
     :tex :eq-shield
     }
   }
  )


