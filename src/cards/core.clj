(ns cards.core
  (:import (org.lwjgl.opengl GL11 GL20))
  (:import (org.lwjgl.input Mouse Keyboard))
  (:use [cards engine shaders gui util])
  (:use [clojure.set])
  )

;(def font (atom 0))



(def LOG-SIZE 10)
(def WIDTH width)
(def HEIGHT height)

;(def CARD-HEIGHT 230)
;(def CARD-WIDTH 159)
(def CARD-RATIO 1.4465408805031446)

(def CARD-WIDTH 159)
(def CARD-HEIGHT (* CARD-WIDTH CARD-RATIO))


(def PATH-CARD-WIDTH 60)
(def PATH-CARD-HEIGHT (* PATH-CARD-WIDTH CARD-RATIO))
(def PATH-CARD-X 30)
(def PATH-CARD-Y 30)

(def HERO-CARD-X 150)

(def HERO-CARD-Y (- HEIGHT 400))
(def HERO-CARD-PADDING-X 10)

;; equipped items
(def EQUIPMENT-CARD-OFFSET-Y 30)

;; items in inventory.
(def INVENTORY-CARD-X (+ 400 (* CARD-WIDTH 3))) ; startx
(def INVENTORY-CARD-Y HERO-CARD-Y) ; starty
(def INVENTORY-CARD-PADDING-X 10)
(def INVENTORY-CARD-PADDING-Y 10)

(def MONSTER-CARD-X 760)
(def MONSTER-CARD-Y 190)
(def MONSTER-CARD-PADDING-X 10)
(def MONSTER-CARD-PADDING-Y 10)
(def MONSTER-CARDS-PER-ROW 4)
;; max number of items in inventory
(def INVENTORY-SIZE 6)

(defn increment-stat[char stat amnt]
  (if (map? amnt)
    (assoc char stat (merge-with + (stat char) amnt))
    (assoc char stat (+ (stat char) amnt)))
    )
(defn decrement-stat[char stat amnt]
  (if (map? amnt)
    (assoc char stat (merge-with - (stat char) amnt))
    (assoc char stat (- (stat char) amnt)))
  )
(defn make-effect [effect name-key duration content src-id]
  {:effect effect :duration duration :key name-key :content content :src-id src-id})

(defn make-damage-component [dmg-dice-num dmg-dice-sides bonus-dmg attack damage-type crit-multiplier crit]
  {:dmg-dice-num dmg-dice-num :dmg-dice-sides dmg-dice-sides :bonus-dmg bonus-dmg :attack attack :damage-type damage-type :crit-multiplier crit-multiplier :crit crit}
  )
(defn set-stat[char stat val]
  (assoc char stat val))

(defn get-item[game id]
  ((:inventory game) id))

(defn set-character[game char]
  (assoc-in game [:characters (:id char)] char))

(def log-string (atom []))
(reset! log-string [])
(defn log[& string]
;  (swap! log-string conj (clojure.string/join " " string))
  (swap! log-string conj
    (get-string-vbo (:celtic-12 @glob-fonts) (clojure.string/join " " string)))
nil
  )

(def monster-id (generate-id))



(defn resolve-attack[attacker defender & {:keys [component] :or{component attacker}}]
  (let [atk-roll (roll 1 20)
        atk (+ atk-roll (:attack component))
        dmg (+ (roll (:dmg-dice-num component) (:dmg-dice-sides component)) (:bonus-dmg component))
        dmg (if (>= atk-roll (:crit component)) (* (:crit-multiplier component) dmg) dmg)]

    (cond

      (>= atk-roll (:crit component))
      (do
        (log (:name attacker) "rolls" atk "and critically hits" (:name defender) "for" dmg "damage!")
        [attacker
         (assoc defender :hp (- (:hp defender) dmg))
         {:result :critical-hit :damage dmg}])

      (>= atk ((:damage-type component) (:resistances defender)))
      (do
        (log (:name attacker) "rolls" atk "and hits" (:name defender) "for" dmg "damage!")
        [attacker (assoc defender :hp (- (:hp defender) dmg)) {:result :hit :damage dmg}])

      (= atk-roll 1)
      (do
        (log (:name attacker) "rolls" atk "and critically misses" (:name defender) ":(")
        [attacker defender {:result :critical-miss :damage dmg}])
      :else
      (do
        (log (:name attacker) "rolls" atk "and misses" (:name defender))
        [attacker defender {:result :miss :damage dmg}])
      )
    )
  )

(defn resolve-heal [component target]
  (let [heal (+ (roll (:heal-dice-num component) (:heal-dice-sides component)) (:bonus-heal component))]
    (log (:name target) "was healed for" heal "hp!")
    (assoc target :hp (if (> (+ heal (:hp target)) (:max-hp target)) (:max-hp target) (+ (:hp target) heal)))))

(defn make-animation[animation animation-type target-id start-time duration param-map]
  {:animation animation
   :animation-type animation-type
   :target target-id
   :start-time start-time
   :end-time (+ start-time duration)
   :duration duration
   :parameters param-map})

(defn add-animation [game animation]
  (assoc game :animations (conj (:animations game) animation) ))

(defn remove-complete-animations[game]

  (let [uncomplete-animations (filter #(<= (:anim-time game) (:end-time %)) (:animations game))]
    (assoc game :animations uncomplete-animations)
    )
  )

(defn animations-done? [game animation-type]
  (let [anims (filter #(= (:animation-type %) animation-type) (:animations game))]
    (if (empty? anims) true false)))

(defn make-animation-component [x y scalex scaley r g b a]
  {:x x :y y :scalex scalex :scaley scaley :rgba [r g b a]})

(defn hit-anim[delta duration param-map]
  (let [freqx 1000
        magnx 100
        freqy 500
        magny 50]
    (make-animation-component
      ( * (* (- duration delta) magnx) (Math/sin (* freqx delta)))
      ( * (* (- duration delta) magny) (Math/sin (* freqy delta)))
    0 0 0 0 0 0
      )
;    [( * (* (- duration delta) magnx) (Math/sin (* freqx delta)))
;     ( * (* (- duration delta) magny) (Math/sin (* freqy delta)))]
    )
  )

(defn attack-anim[delta duration param-map]
  (make-animation-component
    (* (:dirx param-map) (Math/sin (* Math/PI (/ delta duration))))
    (* (:diry param-map) (Math/sin (* Math/PI (/ delta duration))))
    0 0 0 0 0 0
    )
  )
;  [(* (:dirx param-map) (Math/sin (* Math/PI (/ delta duration))))
;   (* (:diry param-map) (Math/sin (* Math/PI (/ delta duration))))]
;  )

(defn dodge-anim[delta duration param-map]
  (make-animation-component
    (* 5 (Math/sin (* Math/PI (/ delta duration))))
    0 0 0 0 0 0 0)
  )
;  [(* 5 (Math/sin (* Math/PI (/ delta duration))))
;   0]
;  )
(defn crit-hit-anim [delta duration param-map]
  (let [freqx 1000
        magnx 120
        freqy 750
        magny 100]
    (make-animation-component
      ( * (* (- duration delta) magnx) (Math/sin (* freqx delta)))
      ( * (* (- duration delta) magny) (Math/sin (* freqy delta)))
      0 0 0 0 0 0
      )
;    [( * (* (- duration delta) magnx) (Math/sin (* freqx delta)))
;     ( * (* (- duration delta) magny) (Math/sin (* freqy delta)))]
    ))



(defn death-anim[delta duration param-map]
  (let [a ( * -1 (/ delta duration))
        a (if (< a -1) -1 a)]
    (make-animation-component 0 0 0 0 0 0 0 a)
    )
  )

(defn skill-anim[delta duration param-map]
  (make-animation-component
    0
    (* 5 (Math/sin (* Math/PI (/ delta duration))))
    0 0 0 0 0 0
    )

;  [0
;   (* 5 (Math/sin (* Math/PI (/ delta duration))))]
  )

(def skill-functions
  {
    :buff (fn [game caster-id targets effect]
            (reduce (fn[game target-id]
                      (let [ char ((:characters game) target-id)
                             new-char (reduce (fn[c key-val]
                                                (cond
                                                  (= (:status effect) :remove)
                                                  (do
                                                    (decrement-stat c (key key-val) (val key-val)))
                                                  (= (:status effect) :add)
                                                  (do
                                                    (increment-stat c (key key-val) (val key-val)))
                                                  :else (do c))
                                                ) char (:content effect))]
                        (set-character game new-char))) game targets))


    :debuff (fn [game caster-id targets effect]
            (reduce (fn[game target-id]
                      (let [ target ((:characters game) target-id)
                             caster ((:characters game) caster-id)
                             new-char (reduce (fn[c key-val]
                                                (cond
                                                  (= (:status effect) :add)
                                                  (do
                                                    (decrement-stat c (key key-val) (val key-val)))
                                                  (= (:status effect) :remove)
                                                  (do
                                                    (increment-stat c (key key-val) (val key-val)))
                                                  :else (do c))
                                                ) target (dissoc (:content effect) :attack-component))
                             game (add-animation game (make-animation hit-anim :skill target-id (get-time) 0.1 {}))]
                        (set-character game new-char))) game targets))

    :damage (fn [game caster-id targets effect]
              (cond
                (or (= (:status effect) :maintain) (= (:duration effect) 0))
                (reduce (fn[game target-id]
                          (let [ target ((:characters game) target-id)
                                 content (:content effect)
                                 atk-def (resolve-attack content target)
                                 result (last atk-def)
                                 game (if (contains? #{:hit :critical-hit} (:result result))
                                        (add-animation game (make-animation hit-anim :skill (:id (second atk-def)) (get-time) 0.1 {}))
                                        (add-animation game (make-animation dodge-anim :skill (:id (second atk-def)) (get-time) 0.1 {}))
                                        )
                                 game (if (<= (:hp (second atk-def)) 0)
                                        (add-animation game
                                          (make-animation death-anim :death (:id (second atk-def)) (+ 0.1 (get-time)) 0.5 {}))
                                        game
                                        )]
                            (-> game
                              ;                            (set-character (first atk-def))
                              (set-character (second atk-def))
                              )
                            )) game targets)

                (= (:status effect) :add)
                game
              :else
              game
                )
              )

    :weapon-damage (fn [game caster-id targets effect]
                     (cond
                       (or (= (:status effect) :maintain) (= (:duration effect) 0))
                       (reduce (fn[game target-id]
                                 (let [ target ((:characters game) target-id)
                                        caster ((:characters game) caster-id)
                                        content (:content effect)
                                        add-stats [:attack :bonus-dmg]
                                        overwrite-stats [:crit-multiplier :crit :dmg-dice-num :dmg-dice-sides :damage-type]
                                        should-add (select-keys content add-stats)
                                        should-overwrite (select-keys content overwrite-stats)
                                        caster-comp (select-keys caster (flatten [add-stats overwrite-stats]))
                                        caster-comp (merge-with + caster-comp should-add)
                                        caster-comp (merge caster-comp should-overwrite)
                                        atk-def (resolve-attack caster target :component caster-comp)
                                        game (if (contains? #{:hit :critical-hit} (:result (last atk-def)))
                                          (add-animation game (make-animation hit-anim :skill (:id (second atk-def)) (get-time) 0.1 {}))
                                          (add-animation game (make-animation dodge-anim :skill (:id (second atk-def)) (get-time) 0.1 {}))
                                          )
                                        game (if (<= (:hp (second atk-def)) 0)
                                          (add-animation game
                                            (make-animation death-anim :death (:id (second atk-def)) (+ 0.1 (get-time)) 0.5 {}))
                                          game
                                          )]
                                   (-> game
                                     (set-character (second atk-def))))) game targets)
                       (= (:status effect) :add)
                       game
                       :else
                       game)
                       )

    :heal (fn [game caster-id targets effect]
            (cond
              (or (= (:status effect) :maintain) (= (:duration effect) 0))
              (reduce (fn[game target-id]
                        (let [ target ((:characters game) target-id)
                               caster ((:characters game) caster-id)
                               content (:content effect)]
                          (-> game
                            (set-character (resolve-heal content target))))) game targets)
              (= (:status effect) :add)
              game
              :else
              game
              )
            )

    :stun (fn [game caster-id targets effect]
            (reduce (fn[game target-id]
                      (let [ target ((:characters game) target-id)]
                        (cond
                          (= (:status effect) :add)
                          (-> game (set-character  (assoc target :stunned true)) (add-animation (make-animation hit-anim :skill target-id (get-time) 0.1 {})))
                          (= (:status effect) :remove)
                          (-> game (set-character  (assoc target :stunned false)) (add-animation (make-animation dodge-anim :skill target-id (get-time) 0.1 {})))
                          :else
                          game
                          )
                        )) game targets))

    :response (fn [game caster-id targets effect]
            (reduce (fn[game target-id]
                      (let [content (:content effect)
                            target ((:characters game) target-id)]
                        (cond
                          (= (:status effect) :add)
                          (set-character game (assoc-in target [(:when content)] (if (empty? ((:when content) target))
                                                                                       (list (:callback content))
                                                                                       (conj ((:when content) target) (:callback content)))))
                          (= (:status effect) :remove)
                          (set-character game (assoc-in target [(:when content)](filter #(= (:name %) (:name effect)) ((:when content) target))))
                          :else
                          game
                          )
                        )) game targets))

    })

(def skills
  {
    :backstab
    {
      :active true
      :stamina-cost 1
      :cooldown 2
      :cooldown-counter 0
      :type :hero-skill
      :target :choose
      :num-targets 1
      :target-type :monster
      :effect :weapon-damage
      :applies :once
      :content {:bonus-dmg 2 :crit-multiplier 3 :crit 15 :attack 5 :damage-type :physical}
      :duration 0
      :text "Quick Weapon Attack with high chance of high damage."
      :name "Backstab"
      }
    :cleave
    {
      :active true
      :stamina-cost 1
      :cooldown 2
      :cooldown-counter 0
      :type :hero-skill
      :target :random
      :num-targets 3
      :target-type :monster
      :effect :weapon-damage
      :applies :once
      :content {:attack 2 :damage-type :physical}
      :duration 0
      :text "Weapon Attack with bonus Attack agains 3 random monsters."
      :name "Cleave"
      }

  :poison-dagger
  {
    :active true
    :stamina-cost 1
    :cooldown 5
    :cooldown-counter 0
    :type :hero-skill
    :target :choose
    :num-targets 1
    :target-type :monster
    :effect :damage
    :applies :repeat
    :content (assoc (make-damage-component 1 4 0 0 :poison 1 20) :name "Poison Dagger")
    :duration 3
    :text "Applies a potent poison, dealing 1d4 Poison Damage for 3 rounds."
    :name "Poison Dagger"
    }

    :berserk
    {
      :active true
      :stamina-cost 1
      :cooldown 10
      :cooldown-counter 0
      :type :hero-skill
      :target :self
      :effect :buff
      :applies :once
      :content {:attack 2 :bonus-dmg 2}
      :duration 5
      :text "The Hero goes nuts! +2 Attack and +2 Damage on Self."
      :name "Berserk"
      }

    :cure-wounds
    {
      :active true
      :stamina-cost 1
      :cooldown 10
      :cooldown-counter 0
      :type :hero-skill
      :target :choose
      :num-targets 1
      :target-type :hero
      :effect :heal
      :applies :once
      :content {:heal-dice-num 1 :heal-dice-sides 8 :bonus-heal 0}
      :duration 0
      :text "Cure 1d8 Hit Points on 1 chosen target."
      :name "Cure Wounds"
      }
    :healing-ward
    {
      :active true
      :stamina-cost 1
      :cooldown 10
      :cooldown-counter 0
      :type :hero-skill
      :target :random
      :num-targets 3
      :target-type :hero
      :effect :heal
      :applies :once
      :content {:heal-dice-num 1 :heal-dice-sides 2 :bonus-heal 1}
      :duration 3
      :text "A protective ward heals all heroes 1d2+1 damage per round for 3 rounds."
      :name "Healing Ward"
      }

    :puncturing-strike
    {
      :active true
      :stamina-cost 1
      :cooldown 5
      :cooldown-counter 0
      :type :hero-skill
      :target :choose
      :num-targets 1
      :target-type :monster
      :effect :debuff
      :applies :once
      :content {;:attack-component (make-damage-component  0 0 0 15 :physical 0 0)
                :resistances {:physical 2}}
      :duration 0
      :text "A strike that weakens the Physical Resistance of 1 Monster target."
      :name "Puncturing Strike"
      }

;    :fireball
;    {
;      :stamina-cost 0
;      :cooldown 5
;      :cooldown-counter 0
;      :type :spell
;      :effect :damage
;      :damage-type #{:magic :fire}
;      :target :all
;      :target-type :monster
;      :content {:dmg-dice-sides 4 :dmg-dice-num 1}
;      :duration 0
;      :name "Fireball"
;      }

    :disarm-traps
    {
      :stamina-cost 0
      :cooldown 0
      :cooldown-counter 0
      :duration 0
      :type :hero-skill
      :active false
      :effect :response
      :content {:when :on-reveal :callback (fn[game card-id]
                                             (let [traps (filter #(= (:sub-type %) :trap) (vals (:characters game)))
                                                   trap ((:characters game) card-id)]
                                               (if (not= (:sub-type trap) :trap)
                                                 game
                                                 (do (log "Trap disarmed!")
                                                  (set-character game (dissoc trap :on-reveal))))))}
      :text "Heroes are unaffected by traps."
      :name "Disarm Traps"
      }

    :stunning-blow
    {
      :active true
      :stamina-cost 1
      :cooldown 5
      :cooldown-counter 0
      :type :hero-skill
      :target :choose
      :target-type :monster
      :num-targets 1
      :effect :stun
      :applies :once
      :content {}
      :duration 2
      :text "Stuns the target for 2 rounds."
      :name "Stunning Blow"
      }
    })

(defn apply-status[game effect target-id]
  (let [v (vec (for [x (range (:duration effect))] (assoc effect :status :maintain)))]
    (if (empty? v)
      game
      (assoc-in game [:characters target-id :status-effects (:key effect)]
        (conj v (assoc effect :status :remove)))
      )
    )
  )

(defn apply-effect [game effect targets]
  (let [
        new-game (reduce (fn [game target-id]
                           (let [target ((:characters game) target-id)
                                 caster ((:characters game) (:source-id effect))
                                 new-game (if (nil? (:attack-component (:content effect)))
                                            (-> (((:effect effect) skill-functions) game (:source-id effect) [target-id] (assoc effect :status :add))
                                              (apply-status effect target-id))
                                            (let [result (resolve-attack caster target :component (:attack-component (:content effect)))]
                                              (if (= (:result (last result)) :hit)
                                                (-> (((:effect effect) skill-functions) game (:source-id effect) [target-id] (assoc effect :status :add))
                                                  (apply-status effect target-id))
                                                (do (log (:name effect) "fails to apply") game)
                                                )
                                              )
                                            )]
                           new-game
                             )

                           ) game targets)
        ]
  new-game

    )
  )

(load "hero-templates")
(load "item-templates")
(load "encounter-templates")

(defn equip-weapon [game hero item]
  (let [hero (increment-stat hero :attack (:attack item))
        hero (set-stat hero :dmg-dice-num (:dmg-dice-num item))
        hero (set-stat hero :dmg-dice-sides (:dmg-dice-sides item))
        hero (set-stat hero :crit (if (nil? (:crit item)) 20 (:crit item)))
        hero (set-stat hero :crit-multiplier (if (nil? (:crit-multiplier item)) 2 (:crit-multiplier item)))
        hero (set-stat hero :damage-type (if (nil? (:damage-type item)) :physical (:damage-type item)))]
    (set-character game hero)
    )
  )

(defn equip-armour [game hero item]
  (let [hero (increment-stat hero :resistances (:resistances item))]
    (set-character game hero)
    )
  )



(defn setup-hero-positions[heroes]
  (for [h (range (count heroes))]
    (assoc (nth heroes h)
      :width CARD-WIDTH :height CARD-HEIGHT
      :posx (+ HERO-CARD-X (* h CARD-WIDTH) (* h HERO-CARD-PADDING-X))
      :posy HERO-CARD-Y :animx 0 :animy 0 :animscalex 1.0 :animscaley 1.0 :animrgba [0 0 0 0])
    )
  )
(defn setup-monster-positions[monsters]
  (for [e (range (count monsters))]
    (assoc (nth monsters e)
      :width CARD-WIDTH :height CARD-HEIGHT
      :posx (+ MONSTER-CARD-X (* (mod e MONSTER-CARDS-PER-ROW) (+ CARD-WIDTH MONSTER-CARD-PADDING-X)))
      :posy (+ MONSTER-CARD-Y (* (dec (Math/ceil (/ (inc e) (double MONSTER-CARDS-PER-ROW)))) (+ CARD-HEIGHT MONSTER-CARD-PADDING-Y)))
      :animx 0 :animy 0 :animscalex 1.0 :animscaley 1.0 :animrgba [0 0 0 0]
      )
    )
  )

(defn setup-item-positions[inventory]
  (for [e (range (count inventory))]
    (assoc (nth inventory e)
     :width CARD-WIDTH :height CARD-HEIGHT
      :posx (+ INVENTORY-CARD-X (* (mod e INVENTORY-SIZE) (+ CARD-WIDTH INVENTORY-CARD-PADDING-X)))                           ; x
      :posy (+ INVENTORY-CARD-Y (* (dec (Math/ceil (/ (inc e) INVENTORY-SIZE))) (+ CARD-HEIGHT INVENTORY-CARD-PADDING-Y)))    ; y
      )
    )
  )


(defn animate[game]
  (let [time (get-time)
        game (reduce (fn [game animation]
                       (let [target ((:characters game) (:target animation))
                             delta (- time (:start-time animation))
                             xy ((:animation animation) delta (:duration animation) (:parameters animation))
                             ]

                         (if (not (nil? target))
                           (if (and (<= time (:end-time animation)) (>= time (:start-time animation)))
                             (set-character game
                               (assoc target
                                 :animx (:x xy)
                                 :animy (:y xy)
                                 :animscalex (:scalex xy)
                                 :animscaley (:scaley xy)
                                 :animrgba (:rgba xy)

                                 )
                               )
;                           game
                             (set-character game
                               (assoc target
                                 :animx 0
                                 :animy 0
                                 :animscalex 0
                                 :animscaley 0
                                 :animrgba [0 0 0 0]
                                 )
                               )
                             )
                         game
                           )
                         )
                       ) game (:animations game))]
    (remove-complete-animations (assoc game :anim-time time))
    )

  )

(defn update-log [game]
  (reset! log-string (vec (take-last LOG-SIZE @log-string)))
  (assoc game :log @log-string)
  )

(defn update [game]
  (let [game (update-log game)
        game (if (empty? (:log game)) game ((:show-log game) game))
        game (update-gui-state game)
        game (animate game)
        game ((:hide-popup game) game)
;        input-state (get-input-state (:input-state game))
;        game (assoc game :input-state input-state)
        ]
    (if (contains? (:keys-pressed (:input-state game)) (Keyboard/KEY_ESCAPE))
      (assoc game :exit true)
      (let [game ((peek (:update-state game)) game)]
        game
        )
      )
    )
  )

(defn draw-monster-card[card]
  (let [x (+ (:posx card) (:animx card))
        y (+ (:posy card) (:animy card))]
;    (GL11/glPushMatrix)
;    (scale (:scalex card) (:scaley card))
  (cond
    (= (:type card) :monster)
    (do
      ;; draw card
      (if (:active card)
        (draw-tex (assoc (:card-sprite @glob-sprites) :posx x :posy y :id (:id card)) (:monster-card @glob-textures) :color-add (:animrgba card))
        (draw-tex (assoc (:card-sprite @glob-sprites) :posx x :posy y :id (:id card)) (:monster-card @glob-textures) :color-add (map + (:animrgba card) [-0.5 -0.5 -0.5 0.0]))
        )
      ;; draw name
      (draw-centered-string (:celtic-12 @glob-fonts) (str (:name card)) (+ x (/ CARD-WIDTH 2.0)) (+ y 12))
      ;; draw damage
      (draw-string (:celtic-12 @glob-fonts) (str (:dmg-dice-num card) "d" (:dmg-dice-sides card)
                                              (if (> (:bonus-dmg card) 0) (str "+" (:bonus-dmg card)) "")) (+ x 40) (+ y 167))
      ;; draw attack
      (draw-string (:celtic-12 @glob-fonts) (str (:attack card)) (+ x 132) (+ y 167))
      ;; draw hp
      (draw-string (:celtic-12 @glob-fonts) (str (:hp card) "/" (:max-hp card)) (+ x 40) (+ y 198))
      ;; draw AC
      (draw-string (:celtic-12 @glob-fonts) (str (:physical (:resistances card))) (+ x 132) (+ y 198))
      )
    (= (:type card) :event)
    (do
      ;; draw card
      (draw-tex (assoc (:card-sprite @glob-sprites) :posx x :posy y :id (:id card)) (:event-card @glob-textures) )
      ;; draw name
      (draw-centered-string (:celtic-12 @glob-fonts) (str (:name card)) (+ x (/ CARD-WIDTH 2.0)) (+ y 12))
      )
  )
;    (GL11/glPopMatrix)
  )
  )
(defn draw-hero-card[card]

  (let [x (+ (:posx card) (:animx card))
        y (+ (:posy card) (:animy card))]

    (if (> (:hp card) 0)
      (do
        ;; draw card
        (if (:active card)
          (draw-tex (assoc (:card-sprite @glob-sprites) :posx x :posy y :id (:id card)) (:hero-card @glob-textures)  :color-add (:animrgba card))
          (draw-tex (assoc (:card-sprite @glob-sprites) :posx x :posy y :id (:id card)) (:hero-card @glob-textures) :color-add [-0.5 -0.5 -0.5 0.0])
          )

        ;; draw name
        (draw-centered-string (:celtic-12 @glob-fonts) (str (:name card)) (+ x (/ CARD-WIDTH 2.0)) (+ y 12))
        ;; draw damage
        (draw-string (:celtic-12 @glob-fonts) (str (:dmg-dice-num card) "d" (:dmg-dice-sides card)
                                                (if (> (:bonus-dmg card) 0) (str "+" (:bonus-dmg card)) "")) (+ x 40) (+ y 167))
        ;; draw attack
        (draw-string (:celtic-12 @glob-fonts) (str (:attack card)) (+ x 132) (+ y 167))
        ;; draw hp
        (draw-string (:celtic-12 @glob-fonts) (str (:hp card) "/" (:max-hp card)) (+ x 40) (+ y 198))
        ;; draw AC
        (draw-string (:celtic-12 @glob-fonts) (str (:physical (:resistances card))) (+ x 132) (+ y 198))
        )

      (do
        ;; draw card
        (draw-tex (assoc (:card-sprite @glob-sprites) :posx x :posy y :id (:id card)) (:death-card @glob-textures))
        ;; draw name
        (draw-centered-string (:celtic-12 @glob-fonts) (str (:name card)) (+ x (/ CARD-WIDTH 2.0)) (+ y 8)))

      )
    )
  )

(defn draw-inventory-card[card]
  (let [x (:posx card)
        y (:posy card)]

    ;; draw card
    (draw-tex (assoc (:card-sprite @glob-sprites) :posx x :posy y :id (:id card)) ((:tex card) @glob-textures))

    (draw-centered-string (:celtic-12 @glob-fonts) (str (:name card)) (+ x (/ CARD-WIDTH 2.0)) (+ y 12))
    (draw-centered-string (:celtic-12 @glob-fonts) (str (:name card)) (+ x (/ CARD-WIDTH 2.0)) (+ y (- CARD-HEIGHT 29)))

    (cond
      (= (:type card) :weapon)
      ;; draw damage
      (do
        (draw-string (:celtic-12 @glob-fonts) (str (:dmg-dice-num card) "d" (:dmg-dice-sides card)
                                                (if (> (:bonus-dmg card) 0) (str "+" (:bonus-dmg card)) "")) (+ x 40) (+ y 167))
        ;; draw attack
        (draw-string (:celtic-12 @glob-fonts) (str (:attack card)) (+ x 132) (+ y 167)))
      ;; draw ac
      (= (:type card) :armour)
      (draw-string (:celtic-12 @glob-fonts) (str (:physical (:resistances card))) (+ x 40) (+ y 167))
      (= (:type card) :shield)
      (draw-string (:celtic-12 @glob-fonts) (str (:physical (:resistances card))) (+ x 40) (+ y 167))
      )
    )

  )

(defn get-equipped-items[hero]
  (sort (filter #(not (nil? %)) (vals (:equipment hero)))))

(defn setup-equipment-positions-relative[x y items]

    (for [i (range (count items))]
      (assoc (nth items i) :posx x :posy (+ y EQUIPMENT-CARD-OFFSET-Y (* i EQUIPMENT-CARD-OFFSET-Y)))
      )

  )

(defn draw-heroes [game interpolation]
  (doseq [n (:unlocked-heroes game)]
    (let [hero ((:characters game) n)
          items (map (:inventory game) (get-equipped-items hero))
          items (reverse (sort-by :posy items))]
      (doseq [p items]
        (draw-inventory-card p))
        (draw-hero-card hero)
      )
    )
  )


(defn draw-monsters[game interpolation]
  (let [startx 760
        starty 190
        paddingh 10
        paddingv 10]
    (doseq [e (range (count (:encounter-area game)))]
      (draw-monster-card ((:characters game) (nth (:encounter-area game) e))
        )
      )
    )
  )
(defn draw-monsters-and-heroes [game interpolation]
  (do (draw-heroes game interpolation) (draw-monsters game interpolation)))

(defn draw-inventory[game interpolation]
  (let [
;         startx INVENTORY-CARD-X
;        starty INVENTORY-CARD-Y
;        paddingh INVENTORY-CARD-PADDING-X
;        paddingv INVENTORY-CARD-PADDING-Y
        equipped-items (reduce #(clojure.set/union %1 (set (vals (:equipment %2)))) #{} (vals (:characters game)))
        inventory (filter #(and (not= (:id %) (:dragging-card game)) (not (contains? equipped-items (:id %)))) (vals (:inventory game)))]

    (doseq [e (range (count inventory))]
      (draw-inventory-card (nth inventory e)
;        (+ startx (* (mod e INVENTORY-SIZE) (+ CARD-WIDTH paddingh)))                           ; x
;        (+ starty (* (dec (Math/ceil (/ (inc e) INVENTORY-SIZE))) (+ CARD-HEIGHT paddingv)))    ; y
        )
      )
    )
  )

(defn draw-initiative-order[game interpolation]
  (let [encounter (:encounter game)
        members (:members encounter)]

    (doseq [x (range (count members))]
      (let [m (nth members x)
            card ((:characters game) (:id m))]
        (when (not (nil? card))
          (draw-string (:celtic-12 @glob-fonts) (str (inc x)) (+ (- CARD-WIDTH 30) (:posx card)) (- (:posy card) 12))
          )

        )
      )

    )
  )

(defn draw-path[game interpolation]
  ;; draw path
  (doseq [p (range (count (:path game)))]
    (let [posx (+ PATH-CARD-X (* p (inc PATH-CARD-WIDTH)))
          posy PATH-CARD-Y
          loc (nth (:path game) p)]
      (cond
        (= (nth (:path game) p) 0)
        (draw-tex (assoc (:path-sprite @glob-sprites) :posx posx :posy posy) (:tavern-card @glob-textures))
        :else
        (do
          (draw-tex (assoc (:path-sprite @glob-sprites) :posx posx :posy posy) (:path-card @glob-textures))
          (draw-string (:celtic-12 @glob-fonts)
            (if (= p (:current-location game)) (str "(" (nth (:path game) p) ")") (str (nth (:path game) p)))
            (+ posx (- (/ (:width (:path-sprite @glob-sprites)) 2) (/ (get-string-width (:celtic-12 @glob-fonts) (str (nth (:path game) p)) ) 2)))
            (+ 50 posy))
          )
        )
      )
    )
  )

(defn default-draw-state[game interpolation]
  (let [
;         game ((:show-log game) game)
         ]
  ;; draw background
  (draw-tex (:bg-sprite @glob-sprites) (:bg @glob-textures))

  (draw-path game interpolation)

  ;; draw heroes
  (draw-heroes game interpolation)

  ;; draw monsters
  (draw-monsters game interpolation)

  ;; draw inventory
  (draw-inventory game interpolation)

  (when (:dragging-card game)
    (draw-inventory-card (assoc ((:inventory game) (:dragging-card game))
      :posx (- (:mousex (:input-state game)) (/ CARD-WIDTH 2))
      :posy (- (- HEIGHT (:mousey (:input-state game))) (/ CARD-HEIGHT 2))
                           )
      )
    )

  ;; draw text
  (draw-string (:celtic-12 @glob-fonts) (str "STAMINA: " (:stamina game)) 1700 20)
  (draw-string (:celtic-12 @glob-fonts) (str "LEVEL: " (:level game)) 1700 40)
  (draw-string (:celtic-12 @glob-fonts) (str "SCORE: " (:overflow game)) 1700 60)
  (draw-string (:celtic-12 @glob-fonts)
      (str "CARDS: "  (count (:encounter-pile game))) 1700 80)
  (draw-string (:celtic-12 @glob-fonts)
      (str "DISCARDED: "  (count (:encounter-discard-pile game))) 1700 100)

    (draw-initiative-order game interpolation)

  (draw-gui game)

;    (draw-str (:celtic-12 @glob-fonts) "HELLo dear world \nhow are you doing tonight." 50 200)
;    (draw-str @font "HELLo dear world how are you doing tonight." 50 225)
;    (draw-str @font "HELLo dear world how are you doing tonight." 50 250)
;    (draw-str @font "HELLo dear world how are you doing tonight." 50 275)
;    (draw-str @font "HELLo dear world how are you doing tonight." 50 300)
;    (draw-str @font "HELLo dear world how are you doing tonight." 50 325)
  )

  )
(defn render [game interpolation]

  (GL11/glClear GL11/GL_COLOR_BUFFER_BIT)
;  (GL11/glLoadIdentity)

  (doseq [state (reverse (:render-state game))]
    (state game interpolation)
    )
  )

(declare draw-stamina-state)
(declare roll-initiative-state)



(defn add-to-encounter-area[game card-id]
  (assoc game :encounter-area (conj (:encounter-area game) card-id)))

(defn remove-from-encounter-area[game card-id]
  (assoc game :encounter-area (filter #(not= % card-id) (:encounter-area game))))

(defn remove-monster[game card-id]
  (let [discard-list (conj (:encounter-discard-pile game) card-id)
        game (assoc game :encounter-discard-pile discard-list)]
    (->
      (assoc game :characters (dissoc (:characters game) card-id))
      (remove-from-encounter-area card-id)
      )
    )

  )

(defn add-hero[game card]
  (-> game
    (set-character card)
;    (assoc :heroes (conj (:heroes game) (:id card)))
    ))

;(load-string (slurp "src/cards/decks.clj"))

(defn get-encounter-cards[game]
  (let [ enc (encounters (:chosen-encounter game))
         enc-l (:encounter enc)
         l (flatten (reduce (fn[encounter-list set]
                               (let [s (encounter-sets set)
                                     card-ids (:cards s)
                                     card-list (map monsters card-ids)
                                     card-list (for [c card-list] (assoc c :id (generate-id)))]
                                 (conj encounter-list card-list)
                                 )
                               ) () enc-l))]
    (reduce #(assoc-in %1 [:encounter-card-list (:id %2)] %2) game l)
    )
  )

(defn draw-cards [card-list discard-list n]
  (loop [n n
         card-list card-list
         discard-list discard-list
         cards ()]
    (if (< n 1)
      {:discard-list discard-list :card-list card-list :cards cards}
      (if (empty? card-list)
        (recur n discard-list [] cards)
        (let [ri (rand-nth (range (count card-list)))
              card-list (vec card-list)
              c (card-list ri)
              ncl (dissoc-idx card-list ri)]
          (recur (dec n) ncl discard-list (conj cards c))
          )
        )
      )
    )
  )

(defn spawn-monsters-state[game]
  (let [game (reduce #(remove-monster %1 %2) game (:encounter-area game))
        draw (draw-cards (:encounter-pile game) (:encounter-discard-pile game) (:level game))
        a (println "DRAW:" draw)
        game (assoc game :encounter-pile (:card-list draw) :encounter-discard-pile (:discard-list draw))
        monsters (map (:encounter-card-list game) (draw :cards))
;        monsters (for [x (range (:level game))] (assoc (rand-nth (:encounter-card-list game)) :id (generate-id)))
        monsters (setup-monster-positions monsters)
        spawned (reduce (fn [game monster]
                   (let [game (-> game
                                (add-to-encounter-area (:id monster))
                                (set-character monster))
                         game (reduce (fn [game hero-id]
                                        (let [hero ((:characters game) hero-id)]
                                          (if (empty? (:on-reveal hero))
                                            game
                                            (reduce (fn [game on-reveal]
                                                      (let [hero ((:characters game) hero-id)]
                                                        (on-reveal game (:id monster))))
                                              game (:on-reveal hero)))
                                          )) game (:unlocked-heroes game))
                         monster ((:characters game) (:id monster))]
                     (if (empty? (:on-reveal monster))
                       game
                       (reduce (fn [game on-rvl]
                                   (on-rvl game (:id monster)))
                         game (:on-reveal monster)))
                     )) game monsters)
        ]

    (set-update-state spawned roll-initiative-state)
    )
  )

(declare player-turn-state)
(declare draw-stamina-state)
(declare start-game-state)

(defn heal-heroes[game]
;  (println (:characters game) (filter #(= (:type %) :hero) (:characters game)))
  (reduce #(set-character %1 (assoc %2 :hp (:max-hp %2))) game (filter #(= (:type %) :hero) (vals (:characters game)))))

(defn draw-end-state[game interpolation]
  (draw-tex (:bg-sprite @glob-sprites) (:bg @glob-textures))
  (draw-centered-string (:celtic-12 @glob-fonts) "CONGRATULATIONS!" (/ WIDTH 2) 300)
  (draw-centered-string (:celtic-12 @glob-fonts) (str "You completed the game with a score of " (:overflow game) "! (lower is better)") (/ WIDTH 2) 351)
  (draw-centered-string (:celtic-12 @glob-fonts) "Press F2 for new game or Esc to exit... " (/ WIDTH 2) 500)
  )

(defn end-game-state[game]
  (if (contains? (:keys-pressed (:input-state game)) (Keyboard/KEY_RETURN))
    (-> game
      (set-update-state start-game-state)
      (pop-render-state))
    game
    )
  )
(defn unlock-next-hero[game]
  (let [heroes (map (:characters game) (:heroes game))
        first-locked (some #(when-not (contains? (:unlocked-heroes game) (:id %)) %) heroes)]
    (assoc-in game [:unlocked-heroes] (conj (:unlocked-heroes game)(:id first-locked)))
;    (set-character game (assoc first-locked :unlocked true))
    )
  )
(defn advance-path-state[game]
  (let [location (nth (:path game) (:current-location game))
        new-location (inc (:current-location game))
        path (filter #(not= % 0) (:path game))
        current (nth path (dec (:level game)))
        new-level (inc (:level game))
        ]
    (if (>= (:stamina game) current)
      (cond
        (>= (:current-location game) (dec (count (:path game))))
        (-> game
          (set-update-state end-game-state)
          (push-render-state draw-end-state))
        (= (nth (:path game) new-location) 0)
        (do
        (-> game
          (heal-heroes)
          (assoc :stamina (+ (:stamina game) 5) :level new-level :current-location (inc new-location))
          (unlock-next-hero)
          (set-update-state player-turn-state)
          ))

        :else
        (-> game
          (assoc :overflow (+ (:overflow game) (- (:stamina game) current)))
          (assoc :stamina 0 :level new-level :current-location new-location)

          (set-update-state player-turn-state)
          )
        )


      (do
        (log "Not enough stamina!")
        (set-update-state game player-turn-state))
      )
    )
  )


(defn apply-item-effects[game char item]
  (cond
    (= (:type item) :weapon)
    (equip-weapon game char item)
    (or (= (:type item) :armour) (= (:type item) :shield))
    (equip-armour game char item)
    :else
    game
    )
  )

(defn equip[game hero-id item-id]
"Equip item. Returns nil if equip failed."
  (let [item (get-item game item-id)
        hero ((:characters game) hero-id)]
    (cond

      (= (:equip item) :1-hand)
      (if (nil? (:hand1 (:equipment hero)))
        (let [hero (assoc-in hero [:equipment :hand1] item-id)]
          (-> game
            (set-character hero)
            (apply-item-effects hero item)
            )
          )
        (if (and (nil? (:hand2 (:equipment hero))) (= (:equip (get-item game (:hand1 (:equipment hero)))) :1-hand))
          (let [hero (assoc-in hero [:equipment :hand2] item-id)]
            (-> game
              (set-character hero)
              (apply-item-effects hero item)
              )
            )
          (do (log "Can't equip" (:name item) "on" (:name hero)) )
          )
        )

      (= (:equip item) :2-hand)
      (if (and (nil? (:hand1 (:equipment hero))) (nil? (:hand2 (:equipment hero))))
        (let [hero (assoc-in hero [:equipment :hand1] item-id)]
          (-> game
            (set-character hero)
            (apply-item-effects hero item)
            )
          )
        (do (log "Can't equip" (:name item) "on" (:name hero)) )
        )

      (= (:equip item) :armour)
      (if (nil? (:armour (:equipment hero)))
        (let [hero (assoc-in hero [:equipment :armour] item-id)]
          (-> game
            (set-character hero)
            (apply-item-effects hero item)
            )
          )
        (do (log "Can't equip" (:name item) "on" (:name hero)) )
        )
      )
    )
  )
(defn show-popup-info [game]
;  (println "popup" (vals (:characters game)))
  (let [target (mouse-select (vals (:characters game)))
        char ((:characters game) target)
        game (reduce #(set-character %1 (assoc ((:characters %1) (:id %2)) :animrgba [0 0 0 0])) game (vals (:characters game)))
        game (if (nil? char)
               game
               (set-character game (assoc char :animrgba [0.15 0.15 0.15 0])))]

    (if (nil? char)
      game
      (do
        ((:show-popup game) game (str (:name char) "\n" (:type char) "\n" (:keywords char))
;                                   (for [kv (dissoc char :name :skill-map :equipment :unlocked
;                                                                      :x :y :id :unlocked :on-reveal :dmg-dice-num
;                                                                      :dmg-dice-sides :attack :hp :max-hp :bonus-dmg :bonus-heal)] kv))
          (:posx char)
          (:posy char)
          )))
    )
  )
(defn show-skill-buttons [game char-id]
  (let [index (first (keep-indexed #(if (= %2 char-id) %1) (:heroes game)))
        pchar ((:characters game) char-id)]
    (if (nil? pchar)
      game
      ((:show-skill-buttons game) game
        (+ HERO-CARD-X 20 (* index (+ CARD-WIDTH HERO-CARD-PADDING-X)))
        (- HERO-CARD-Y 27)
        (filter #(not (nil? %)) (vals (:skill-map pchar)))))
    )
  )

(defn init-skill-buttons[game]
  (let [game (reduce
               #(let [game (show-skill-buttons %1 %2)]
                  ((:disable-skill-buttons game) game)
                 )
               game
               (:heroes game)

               )]
game
    )
  )
(defn set-active [game active? hero-ids]
  (reduce #(set-character %1 (assoc ((:characters %1) %2) :active active?)) game hero-ids)
  )

;; draw the player's full inventory in the middle of the screen
(defn draw-inventory-active[game interpolation])

;; draw compactly the player's inventory in the bottom right of the screen
(defn draw-inventory-inactive[game interpolation])

;; the state in which the player's active inventory is drawn ontop of the scene
(defn draw-inventory-state[game interpolation])

;; enables the player to equip items by dragging them onto the characters
(defn show-inventory-state[game])

(defn player-turn-state[game]
  (let [game ((:show-choice game) game)
        game (init-skill-buttons game);((:disable-skill-buttons game) game)
        game (set-active game true (:heroes game))
        ]
    (cond
      (:spawn-monsters game)
      (-> ((:hide-choice game)(dissoc game :spawn-monsters))
        (set-update-state spawn-monsters-state)

        )

      (:progress game)
      (-> ((:hide-choice game)(dissoc game :progress))
        (set-update-state advance-path-state)
        )

      (:dragging-card game)
      (cond
        (contains? (:mouse-released (:input-state game)) 0)
        (let [target-card-id (mouse-select (map (:characters game) (:heroes game)))
              target-card ((:characters game) target-card-id)]
          (if (nil? target-card)
            (assoc game :dragging-card false)
            (let [new-game (equip game target-card-id (:dragging-card game))]
              (if (nil? new-game)
                (assoc game :dragging-card false)
                (let [;a (println "GAME" new-game)
                      hero ((:characters new-game) target-card-id)
                      items (get-equipped-items hero)

;                      items (conj items ((:inventory game) (:dragging-card game)))
                      item-pos (setup-equipment-positions-relative (:posx hero) (:posy hero) (map (:inventory new-game) items))
                      new-game (reduce #(-> %1
                                          (assoc-in [:inventory (:id %2)] %2)
                                          (assoc-in [:inventory (:id %2)] %2)
                                          ) new-game item-pos)
                      ]
                  (assoc new-game :dragging-card false)
                  )
                )
              )
            )
          )
        :else
        game
        )

    :else
      (cond
        (contains? (:mouse-pressed (:input-state game)) 0)
        (let [id (mouse-select (vals (:inventory game)))
              card ((:inventory game) id)]
          (if (nil? card)
            game
            (assoc game :dragging-card id)
            )
          )

        :else
        (show-popup-info game)
        )
      )
    )
  )

(declare next-character-state)

(defn remove-dead-monsters[game]

  "remove dead monsters and add stamina for each killed"
  (let [dead-monsters (filter #(and (= (:type %) :monster) (<= (:hp %) 0)) (vals (:characters game)))
        new-game (reduce #(do
                            (log "Killed monster, gained stamina!")
                            (remove-monster (assoc %1 :stamina (+ (:stamina %1) 2)) (:id %2)))

                   game dead-monsters)]

    new-game)
  )

(defn check-battle-over-state[game]
  (if (animations-done? game :death)
    (let [game (remove-dead-monsters game)]
      (cond
        (empty? (filter #(and (= (:type %) :hero) (> (:hp %) 0)) (map (:characters game) (:unlocked-heroes game))))
        (set-update-state game start-game-state)

        (empty? (filter #(= (:type ((:characters game) %)) :monster) (:encounter-area game)))
        (set-update-state game player-turn-state)

        :else
        (set-update-state game next-character-state))
      )
    game
    )

  )





(defn perform-skill [game skill targets]
  (let [new-game (apply-effect game skill targets)
        new-game (assoc-in new-game [:characters (:source-id skill) :skill-map (:id skill)]
                   (assoc skill :cooldown-counter (:cooldown skill)))
        ]
    (assoc new-game :stamina (- (:stamina new-game) (:stamina-cost skill)))
    )
  )



(defn generate-skill-animations[game]
  (let [skill (:skill (:target-state game))
        caster (:caster (:target-state game))
        anim-delay 0
        anim-duration 0.2
        ]

    (add-animation game
      (make-animation skill-anim :skill (:id caster) (+ anim-delay (get-time)) anim-duration {}))

    )
  )
(defn update-skills-state[game]
  (if (animations-done? game :skill)
    (set-update-state game check-battle-over-state)
  game
    )
  )

(defn perform-skill-state[game]
  (let [skill (:skill (:target-state game))
        targets (:targets (:target-state game))
        caster ((:characters game) (:source-id skill))
        game (assoc-in game [:target-state :caster] caster)
        new-game (perform-skill game skill targets)
        new-game (assoc new-game :stamina (- (:stamina new-game) (:stamina-cost skill)))
        ]
    (-> new-game
;      (assoc-in [:skill-state] new-game)
      (generate-skill-animations)
;      (remove-dead-monsters)
      (set-update-state update-skills-state)
;      (push-update-state play-skill-animations-state)
      )
  )
  )

(defn skill-target-state[game]
  (let [game (show-popup-info game)]
    (if (= (count (:targets (:target-state game))) (:num-targets (:skill (:target-state game))))

      (-> game
        (set-update-state perform-skill-state)
        )

      (if (contains? (:mouse-released (:input-state game)) 0)
        (if (< (count (:targets (:target-state game))) (:num-targets (:skill (:target-state game))))
          (cond
            (= (:target-type (:skill (:target-state game))) :monster)
            (let [target-id (mouse-select (vals (:characters game)))
                  monster ((:characters game) target-id)]
              (if (or (nil? monster) (not (= (:type monster) :monster)))
                game
                (do
                  (assoc-in game [:target-state :targets] (conj (:targets (:target-state game)) (:id monster))))
                )
              )
            (= (:target-type (:skill (:target-state game))) :hero)
            (let [target-id (mouse-select (vals (:characters game)))
                  hero ((:characters game) target-id)]
              (if (or (nil? hero) (not (= (:type hero) :hero)))
                game
                (do
                  (assoc-in game [:target-state :targets] (conj (:targets (:target-state game)) (:id hero))))
                )
              )
            :else
            game
            )
          (do (log "Invalid number of targets selected!") game)
          )
        game
        )
      )
    )
  )

(defn generate-attack-animations[game]
  (let [attacker (:attacker (:attack-state game))
        defender (:defender (:attack-state game))
        attack-result (:result (:result (:attack-state game)))
        time (get-time)
;        delta (- (get-time) time)
        defense-delay (if (= (:type attacker) :monster) 0.35 0.15) ;; delay before hit/dodge anim starts
        hit-duration (cond
                       (= attack-result :miss) 0.1
                       (= attack-result :hit) 0.1
                       (= attack-result :critical-hit) 0.5
                       (= attack-result :critical-miss) 0.1)  ;; duration of the hit anim
        attack-delay (if (= (:type attacker) :monster) 0.25 0.05) ;; delay before attack anim starts
        attack-duration 0.1 ;; duration of the attack anim

        hit-animation  (cond
                         (= attack-result :miss)
                         (make-animation dodge-anim :attack (:id defender) (+ time defense-delay) hit-duration {})
                         (= attack-result :hit)
                         (make-animation hit-anim :attack (:id defender)   (+ time defense-delay) hit-duration {})
                         (= attack-result :critical-hit)
                         (make-animation crit-hit-anim :attack (:id defender) (+ time defense-delay) hit-duration {})
                         (= attack-result :critical-miss)
                         (make-animation dodge-anim :attack (:id defender) (+ time defense-delay) hit-duration {}))

        attack-animation (make-animation attack-anim :attack (:id attacker) (+ time attack-delay)
                           attack-duration
                           (if (= (:type attacker) :monster) {:dirx -10 :diry 10} {:dirx 10 :diry -10}))

        ]
    (-> game (add-animation hit-animation) (add-animation attack-animation))

    )
  )

(defn update-attack-state[game]
  (if (animations-done? game :attack)
    (let [attacker (:attacker (:attack-state game))
          defender (:defender (:attack-state game))
          result (:result (:attack-state game))
          new-game (-> game
                     (set-character defender)
                     (set-character attacker))]
      ;            (play-sound (rand-nth [:flip1 :flip2 :flip3 :flip4]) :loop false)
      (if (> (:hp defender) 0)
        (set-update-state new-game next-character-state)
        (do
          (log (:name defender) "is killed.")
          (-> new-game
            (add-animation
              (make-animation death-anim :death (:id defender) (get-time) 0.5 {}))
            (set-update-state check-battle-over-state)
            )
          )
        )
      )
  game
    )


  )
(defn resolve-attack-state[game]
  (let [attacker (:attacker (:attack-state game))
        defender (:defender (:attack-state game))
        atk-def (resolve-attack attacker defender)]
    (-> (assoc-in game [:attack-state :defender] (second atk-def))
      (assoc-in [:attack-state :attacker] (first atk-def))
      (assoc-in [:attack-state :result] (last atk-def))
      (generate-attack-animations)
      (set-update-state update-attack-state)
      )
    )
  )

(defn player-choose-target-state[game]
  (let [game (set-active game true (:encounter-area game))]
    (if (:skill game)
      (let [skill (:skill game)
            game (assoc game :skill nil)]
        (cond

          (< (:stamina game) (:stamina-cost skill))
          (do (log "Not enough stamina to use skill!") game)

          (not= (:cooldown-counter skill) 0)
          (do (log "Skill is on cooldown!") game)

          :else
          (cond
            (= (:target skill) :self)
            (-> game
  ;            (perform-skill skill [(:source-id skill)])
  ;            (remove-dead-monsters)
              (assoc-in [:target-state :skill] skill)
              (assoc-in [:target-state :targets] [(:source-id skill)])
              (set-update-state perform-skill-state))

            (= (:target skill) :choose)
            (do (log "Choose" (:num-targets skill) "target(s) of type" (:target-type skill))
              (set-update-state (assoc game :target-state {:skill skill :targets []}) skill-target-state))

            (= (:target skill) :random)
            (let [targets (map :id (filter #(= (:type %) (:target-type skill)) (vals (:characters game))))]
              (if (empty? targets)
                (do (log "No valid targets!") game)
                (-> game
                  (assoc-in [:target-state :skill] skill)
                  (assoc-in [:target-state :targets] (pick-n-random (:num-targets skill) targets))
                  (set-update-state perform-skill-state))
                )
              )
            :else
            game
            )
          )
        )


      (if (contains? (:mouse-released (:input-state game)) 0)
        (let [pchar ((:characters game) (:id (nth (:members (:encounter game)) (:current-char (:encounter game)))))
              target (mouse-select (vals (:characters game)))
              mchar ((:characters game) target)]

          (cond
            (or (= target -1) (= (:type mchar) :event))
            game

            (nil? mchar)
            game

            (= (:type mchar) :monster)
            (-> (set-update-state
                  (-> game
                    (assoc-in [:attack-state :attacker] pchar)
                    (assoc-in [:attack-state :defender] mchar))
                  resolve-attack-state)
              )

          :else
          game
            )
          )
        (show-popup-info game)
        )
      )
    )
  )

;(defn update-skill-effects-state[game]
;  (let [skill-result (:result (:skill-state game))
;        targets (:targets skill-result)
;        affected-targets (:affected-targets skill-result)
;        effect (:effect skill-result)]
;    (reduce #(set-character %1 (apply-status effect %2) game affected-targets))
;    )
;  )

(defn update-status-effects[game char-id]
            (let [char ((:characters game) char-id)]
              (reduce (fn[game status-key-val]
                        (let [effect-key (key status-key-val)
                              effect (first (val status-key-val))
                              char ((:characters game) char-id)]
                          (if (nil? effect)
                            (set-character game (assoc-in char [:status-effects] (dissoc (:status-effects char) effect-key)))
                            (let [game (((:effect effect) skill-functions) game (:source-id effect) [char-id] effect)
                                  char ((:characters game) char-id)]
                              (set-character game (assoc-in char [:status-effects effect-key] (subvec (effect-key (:status-effects char)) 1)))
                              )
                            )
                          )
                        ) game (:status-effects char))
              )
            )

(defn update-cooldowns [game char-id]
  (let [char ((:characters game) char-id)]
    (reduce (fn [game skill]
              (let [char ((:characters game) char-id)
                     cdc (dec (:cooldown-counter skill))]
                (if (<= cdc 0)
                  (set-character game (assoc-in char [:skill-map (:id skill) :cooldown-counter] 0))
                  (set-character game (assoc-in char [:skill-map (:id skill) :cooldown-counter] cdc))
                  )
                )
              ) game (vals (:skill-map char)))))

(defn update-character[game char-id]
  (let [char ((:characters game) char-id)
        status-names (vec (map :name (vals (select-keys skills (keys (:status-effects char))))))
        game (update-status-effects game char-id)
        game (update-cooldowns game char-id)]
    (when-not (empty? status-names) (log (:name char) "is under the effects of" status-names))
    game
    )
  )

(defn next-character-state[game]
  (let [game ((:disable-skill-buttons game) game)
        enc (:encounter game)
        cc (inc (:current-char enc))
        cc (if (>= cc (count (:members enc))) 0 cc)
        enc (assoc enc :current-char cc)
        curr-char (nth (:members enc) (:current-char enc))
        real-char ((:characters game) (:id curr-char))
        game (if (or (= (:type real-char) :monster) (= (:type real-char) :hero))
               (update-character game (:id real-char))
                game)
        game (remove-dead-monsters game)
        real-char ((:characters game) (:id curr-char))
        new-game (assoc game :encounter enc)
        ]
    (cond

      ;; monsters are removed from the game when they are killed, so we need to check if the real character exists.
      (nil? real-char)
      (do (log (:name curr-char) "is DEAD.") (set-update-state new-game check-battle-over-state))

      (:stunned real-char)
      (do (log (:name curr-char) "is stunned.") new-game)

      (and (= (:type curr-char) :monster) (> (:hp real-char) 0))
      (let [ a (log (str (:name curr-char) "..." ))
             new-game (set-character new-game (assoc ((:characters new-game) (:id curr-char)) :scalex 1.5 :scaley 1.5))
             attacker ((:characters new-game) (:id curr-char))
             defender (rand-nth (filter #(and (contains? (:unlocked-heroes new-game) (:id %)) (> (:hp %) 0))
                                  (vals (select-keys (:characters new-game) (:heroes new-game)))))]

        (-> (set-update-state
              (-> new-game
                (set-active false (:encounter-area game))
                (set-active true [(:id curr-char)])
                (assoc-in [:attack-state :attacker] attacker)
                (assoc-in [:attack-state :defender] defender)) resolve-attack-state)
;          (assoc-in [:attack-state :time] (. lwjgl-timer getTime))
          )

        )
      (and (= (:type curr-char) :hero) (> (:hp real-char) 0))
      (do (log (str (:name curr-char) "..." ))
        (-> new-game
          (set-active false (:heroes new-game))
          (set-active true [(:id curr-char)])
          (show-skill-buttons (:id curr-char))
          (set-update-state player-choose-target-state)
          )
        )

      ;; heroes are not removed from the game when they are killed
      :else
      (do (log (:name curr-char) "is DEAD.") (set-update-state new-game check-battle-over-state))
      )

    )
  )


; typ, id, hp, initiativ
(defn roll-initiative-state[game]
  (let [game (set-active game false (:unlocked-heroes game))
        members (for [char (flatten [(vec (:unlocked-heroes game)) (:encounter-area game)])]
                  (let [c ((:characters game) char)]
                    (println (:name c))
                    (when (contains? #{:monster :hero} (:type c))
                      (assoc (select-keys c [:id :type :hp :initiative :name]) :rolled-initiative (+ (roll 1 20) (:initiative c)))
                      )
                    )
                  )
        members (filter #(not= % nil) members)
        members (reverse (sort-by :rolled-initiative members))
        a (when-not (empty? (filter #(= (:type %) :monster) members))
          (do
            (log "Initiative order:")
            (doseq [m members]
              (log (:name m) (str " (rolled " (:rolled-initiative m) ")"))
              )
            )
          )

        encounter {:members members
                   :current-char -1 ;; index into members
                   :round 0
                   }
       ]

    (if (empty? (filter #(= (:type %) :monster) members))
      (set-update-state (assoc game :encounter encounter) player-turn-state)
      (set-update-state (assoc game :encounter encounter) next-character-state)
      )

  )
  )

(defn apply-passive-skills[game target-ids]
  (reduce (fn [game target-id]
            (let [char ((:characters game) target-id)]
              (reduce (fn [game sk]
                        (if-not (:active sk)
                          (apply-effect game sk (list (:id char)))
                          game
                          )
                        ) game (vals (:skill-map char))))) game target-ids)
  )

(defn init-heroes[game]
  (reduce (fn[game hero]
            (let [hero (assoc hero
                         :equipment
                         {:head nil :armour nil :hand1 nil :hand2 nil :legs nil :ring1 nil :ring2 nil :amulet nil}
                         :status-effects {})]
              (add-hero game
                (reduce (fn[hero skill-key]
                          (let [skill (skill-key skills)
                                skill (assoc skill :id (generate-id) :type :skill :source-id (:id hero) :key skill-key)
                                new-hero (assoc-in hero [:skill-map (:id skill)] skill)]
                            new-hero
                            )) hero (:skills hero))))
            ) game (setup-hero-positions (map heroes (:heroes game))))
  )

(defn init-inventory[game]
  (assoc game :inventory
    (reduce #(assoc %1 (:id %2) %2) {}
      (setup-item-positions (for [e (:starting-inventory game)]
                              (assoc (items e) :id (generate-id))))))
  )

(defn init-encounter-sets[game]
  (let [game (get-encounter-cards game)]
    (assoc game :encounter-pile (keys (:encounter-card-list game)))
    )
  )

(def equipment [1 1 1 2 3 4])


(defn create-game[]
  (reset! log-string [])
  (-> {}
  (assoc
    :on-update update
    :on-render render
    :update-state ()
    :render-state ()

    :encounter-area [] ; array of id's
    :chosen-encounter 1 ; encounter id
    :encounter-card-list {} ;; complete map of encounter card id's -> card
    :encounter-pile [] ;; array of undrawn card id's
    :encounter-discard-pile [] ;; array of discarded card id's

    :characters {}
    :heroes [1 2 3]
    :stamina 0
    :threat 0
    :level 1
    :current-location 1
    :unlocked-heroes #{}
    :starting-inventory [1 1 1 2 3 4]


    :input-state (make-input-state)
    :path [0 2 3 0 4 5 3 0 2 5 2]
;    :path [0 2 3 0 4 5 3]
    :overflow 0
    )
    (init-heroes)
    (init-inventory)
    (init-encounter-sets)
    (create-gui)
    )
  )

(defn start-game-state[game]
  (-> (create-game)
      (unlock-next-hero)
      (apply-passive-skills (:heroes game))
      (set-update-state player-turn-state)
      (set-render-state default-draw-state)
    )
  )

;(defn test-state [game]
;  game)
;
;(defn draw-test-state[game interpolation]
;  (draw-blur game)
;  )

(defn init-resources[]
  (swap! glob-sprites assoc :card-sprite (assoc (create-base-sprite CARD-WIDTH CARD-HEIGHT) :id 0))
  (swap! glob-sprites assoc :skill-sprite (assoc (create-base-sprite 25 25) :id 0))
  (swap! glob-sprites assoc :path-sprite (assoc (create-base-sprite PATH-CARD-WIDTH PATH-CARD-HEIGHT) :id 0))
  (swap! glob-sprites assoc :bg-sprite (assoc (create-base-sprite WIDTH HEIGHT) :id 0))
  (swap! glob-sprites assoc :btn (assoc (create-base-sprite 84 84) :id 0))



  (swap! glob-textures assoc :advance-btn1 (create-texture "resources/gfx/advance_btn1.png"))
  (swap! glob-textures assoc :advance-btn2 (create-texture "resources/gfx/advance_btn2.png"))
  (swap! glob-textures assoc :engage-btn1 (create-texture "resources/gfx/engage_btn1.png"))
  (swap! glob-textures assoc :engage-btn2 (create-texture "resources/gfx/engage_btn2.png"))

  (swap! glob-textures assoc :hero-card (create-texture "resources/gfx/hero_card.png"))
  (swap! glob-textures assoc :monster-card (create-texture "resources/gfx/monster_card.png"))
  (swap! glob-textures assoc :tavern-card (create-texture "resources/gfx/tavern_card.png"))
  (swap! glob-textures assoc :event-card (create-texture "resources/gfx/event_card.png"))
  (swap! glob-textures assoc :path-card (create-texture "resources/gfx/path_card2.png"))
  (swap! glob-textures assoc :death-card (create-texture "resources/gfx/death_card.png"))
  (swap! glob-textures assoc :bg (create-texture "resources/gfx/board_bg1920.png"))

  (swap! glob-textures assoc :eq-shield (create-texture "resources/gfx/eq_shield.png"))
  (swap! glob-textures assoc :eq-dagger (create-texture "resources/gfx/eq_dagger.png"))
  (swap! glob-textures assoc :eq-bow (create-texture "resources/gfx/eq_bow.png"))
  (swap! glob-textures assoc :eq-armour (create-texture "resources/gfx/eq_armour.png"))

  (swap! glob-textures assoc :skill-btn1 (create-texture "resources/gfx/skill_btn1.png"))
  (swap! glob-textures assoc :skill-btn2 (create-texture "resources/gfx/skill_btn2.png"))

;  (swap! glob-shader assoc :test-hor (create-shader-program "hor-blur"))
;  (swap! glob-shader assoc :test-ver (create-shader-program "vert-blur"))




;  (swap! glob-music assoc :wos (load-streaming-audio "OGG" "resources/sound/wos.ogg"))
;
;  (swap! glob-sound-effects assoc :flip1 (load-audio "WAV" "resources/sound/effects/cardShove1.wav"))
;  (swap! glob-sound-effects assoc :flip2 (load-audio "WAV" "resources/sound/effects/cardShove1.wav"))
;  (swap! glob-sound-effects assoc :flip3 (load-audio "WAV" "resources/sound/effects/cardShove1.wav"))
;  (swap! glob-sound-effects assoc :flip4 (load-audio "WAV" "resources/sound/effects/cardShove1.wav"))


  )



(declare setup-main-menu start-game exit-game)

(defn setup-encounter-selection-menu[game]
  (let [encs (vals encounters)
        menu-items (for [i (range (count encs))]
                     (do ;(println (nth encs i))
                         (merge (nth encs i) {:posx 500 :posy (+ 500 (* (inc i) 55)) :width 250 :height 50})))
        clickies (reduce #(assoc %1 (:id %2) (assoc %2
                                               :text (:name %2)
                                               :action (fn[game] (assoc (start-game game) :chosen-encounter (:id %2)))
                                               )) {} menu-items)]

    (assoc game :clickies (assoc clickies 10 {:posx 500 :posy (+ 500 (* (inc (count clickies)) 55)) :width 250 :height 50 :text "Back" :id 10
                                                 :action setup-main-menu}))
    )
  )

(defn setup-profile-menu[game]
  (assoc game :clickies {  1 {:posx 500 :posy 500 :width 250 :height 50 :text "Ulf" :id 1
                              :action (fn[game]game)}
                           2 {:posx 500 :posy 555 :width 250 :height 50 :text "Back" :id 2
                              :action setup-main-menu}

                           }
    )
  )


(defn setup-main-menu[game]
  (assoc game :clickies {  1 {:posx 500 :posy 500 :width 250 :height 50 :text "Map" :id 1
                              :action setup-encounter-selection-menu}
                           2 {:posx 500 :posy 555 :width 250 :height 50 :text "Profile" :id 2
                              :action setup-profile-menu}
                           3 {:posx 500 :posy 610 :width 250 :height 50 :text "Exit" :id 3
                              :action exit-game}
                          }
    )
  )

(def profile (atom {}))

(defn load-profile[name])

(defn menu-update[game]
  (let [game (reduce #(assoc-in %1 [:clickies (:id %2) :hover] false) game (vals (:clickies game)))
        ent (mouse-select (vals (:clickies game)))]
    (if (nil? ent)
      game
      (if (contains? (:mouse-released (:input-state game)) 0)
        ((:action ((:clickies game) ent)) game)
        (assoc-in game [:clickies ent :hover] true)
        )
      )
    )
  )

(defn menu-render[game interpolation]
  (draw-tex (:bg-sprite @glob-sprites) (:bg @glob-textures))
  (doseq [c (vals (:clickies game))]
;    (draw-quad (:posx c) (:posy c) (:width c) (:height c) :r 1 :g 1 :b 1 :a 0.1)
    (if (:hover c)
      (draw-string (:celtic-42 @glob-fonts) (:text c) (+ 0 (:posx c)) (+ 0 (:posy c)) :r 0.5)
      (draw-string (:celtic-42 @glob-fonts) (:text c) (+ 0 (:posx c)) (+ 0 (:posy c)))
      )
    )
  )

(defn start-game[game]
  (-> (create-game)
    (set-update-state start-game-state)
    (push-render-state default-draw-state))
  )
(defn exit-game[game]
  (assoc game :exit true))

(defn main[]
  (init-gl)
;  (reset! font (load-font-xml "resources/fonts/test.fnt" "resources/fonts/test_0.png"))
  (create-fonts)
  (init-resources)
;  (play-music :wos)
  (let [
;         game (create-game)
;        fbs (init-fbo-sprite-tex 100 100)
;        a (println fbs)
        ]
    (->
;      (create-game)
      {:on-update menu-update
       :on-render menu-render
       :input-state (make-input-state)
       }
      (setup-main-menu)
;      (assoc :fbo fbs)

;      (run update menu-render)
;      (assoc :exit false)
;      (set-update-state start-game-state)
;      (push-render-state default-draw-state)
      (run)
      )
    )
;  (swap! glob-sprites assoc :test-sprite (assoc (create-base-sprite CARD-WIDTH CARD-HEIGHT) :id 0))
;  (swap! glob-textures assoc :test-card (create-texture "resources/gfx/hero_card.png"))
;  (swap! glob-shader assoc :test-shader (create-shader-program "test")
;    )(println (.getWidth (:tex (:test-card @glob-textures))))
;  (run {} test-update test-render)
  )

(main)