(ns cards.util)

(defn index-exclude [r ex]
  "Take all indices execpted ex"
  (filter #(not (ex %)) (range r)))

(defn dissoc-idx [v & ds]
  (map v (index-exclude (count v) (into #{} ds))))


(defn roll [num-dice sides]
  (reduce #(+ %2 %1) 0 (for [x (range num-dice)] (inc (rand-int sides))))
  )

(defn pick-n-random [n list]
  (loop [targets list rand-targets ()]
    (if (or (= (count rand-targets) n) (empty? targets))
      rand-targets
      (let [r (rand-nth targets)]
        (recur (filter #(not= r %) targets) (conj rand-targets r))))))