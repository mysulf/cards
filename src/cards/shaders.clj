(ns cards.shaders
  (:import (org.lwjgl.opengl GL11 GL20 ARBVertexShader ARBFragmentShader ARBShaderObjects)))


(defn compile-errors [shader-id]
  (ARBShaderObjects/glGetInfoLogARB shader-id 1000))

(defn compile-shader[shader filename]
  (let [code (slurp filename)]
    (do
      (ARBShaderObjects/glShaderSourceARB shader code)
      (ARBShaderObjects/glCompileShaderARB shader)
      (when (> (count (compile-errors shader))
              0)
        (println (compile-errors shader)))
      )
    shader))

(defn create-frag-shader[filename]
  (let [frag-shader (ARBShaderObjects/glCreateShaderObjectARB ARBFragmentShader/GL_FRAGMENT_SHADER_ARB)]
    (compile-shader frag-shader filename)))

(defn create-vert-shader[filename]
  (let [vert-shader  (ARBShaderObjects/glCreateShaderObjectARB ARBVertexShader/GL_VERTEX_SHADER_ARB)]
    (compile-shader vert-shader filename)))

(defn create-shader-program[shader-file]
  (let [shader (ARBShaderObjects/glCreateProgramObjectARB)
        vert-shader (create-vert-shader (str "resources/shaders/" shader-file ".v.glsl"))
        frag-shader (create-frag-shader (str "resources/shaders/" shader-file ".f.glsl"))]
    (ARBShaderObjects/glAttachObjectARB shader vert-shader)
    (ARBShaderObjects/glAttachObjectARB shader frag-shader)


    (ARBShaderObjects/glLinkProgramARB shader)
    (ARBShaderObjects/glValidateProgramARB shader)
    (when (> (count (compile-errors shader))
            0)
      (println (compile-errors shader)))

    shader))

(defn load-shader[shader]
  (ARBShaderObjects/glUseProgramObjectARB shader))

(defn get-uniform-location[shader uniform-name]
  (GL20/glGetUniformLocation shader uniform-name)
  )

(defn run-circle-shader[shader tex]

  (load-shader shader)

  (GL11/glBindTexture GL11/GL_TEXTURE_2D (.getTextureID tex))

  (let [tex-loc    (GL20/glGetUniformLocation shader "tex")
        border-loc (GL20/glGetUniformLocation shader "border")
        radius-loc (GL20/glGetUniformLocation shader "circle_radius")
        color-loc  (GL20/glGetUniformLocation shader "circle_color")
        center-loc (GL20/glGetUniformLocation shader "circle_center")]

    (GL20/glUniform1i tex-loc 0)

    (GL20/glUniform1f border-loc 0.01)
    (GL20/glUniform4f color-loc 1.0 0.0 0.0 1.0)
    (GL20/glUniform2f center-loc (/ (.getWidth tex) 2) (/ (.getHeight tex) 2))
    (GL20/glUniform1f radius-loc 0.5)

    )
  )

(defn release-shader[]
  (ARBShaderObjects/glUseProgramObjectARB 0))