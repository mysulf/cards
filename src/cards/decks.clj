;(ns cards.core)
;
;(defn make-effect [effect name-key duration content src-id]
;  {:effect effect :duration duration :content content :src-id src-id :key name-key})



(def encounter-sets {
                      :goblin-menace {
                                       :name "Goblin Menace"
                                       :text "The immigrant goblins from the north, or \"forest goblins\", arrived some
                                       hundred years ago. While not particularly deadly in combat, their sneaky ways have
                                       lured countless of unbeknownst peasants and village people deep into the woods, ripe
                                       for mugging and thieving."
                                       :numbers {:goblin-scout 3 :goblin-hunter 3 :goblin-raider 2 :spike-trap 1 :goblin-feast 1}
                                       :cards {
                                                :goblin-hunter
                                                {
                                                  :type :monster
                                                  :name "Goblin Hunter"
                                                  :dmg-dice-num 1
                                                  :dmg-dice-sides 2
                                                  :crit-multiplier 2
                                                  :crit 20
                                                  :damage-type :physical
                                                  :resistances {:physical 12 :magic 0 :fire 0 :poison 0 :thorns 0}
                                                  :attack 2
                                                  :hp 6
                                                  :max-hp 6
                                                  :bonus-dmg 0
                                                  :initiative 7
                                                  }

                                                :goblin-scout
                                                {
                                                  :type :monster
                                                  :name "Goblin Scout"
                                                  :dmg-dice-num 1
                                                  :dmg-dice-sides 2
                                                  :crit-multiplier 2
                                                  :crit 20
                                                  :damage-type :physical
                                                  :resistances {:physical 10 :magic 0 :fire 0 :poison 0 :thorns 0}
                                                  :attack 1
                                                  :hp 4
                                                  :max-hp 4
                                                  :bonus-dmg 0
                                                  :initiative 2
                                                 }

                                                 :goblin-raider
                                                 {
                                                  :type :monster
                                                  :keywords #{:goblin}
                                                  :name "Goblin Raider"
                                                  :dmg-dice-num 1
                                                  :dmg-dice-sides 3
                                                  :crit-multiplier 2
                                                  :crit 19
                                                  :damage-type :physical
                                                  :resistances {:physical 12 :magic 0 :fire 0 :poison 0 :thorns 0}
                                                  :attack 1
                                                  :hp 5
                                                  :max-hp 5
                                                  :bonus-dmg 0
                                                  :initiative 2
                                                  }

                                                  :goblin-feast
                                                  {
                                                  :type :event
                                                  :sub-type :buff
                                                  :name "Goblin Feast"
                                                  :text "Goblins everywhere. Limbs and intestines of something unknown, foul smells
                                                   of rotten excitement."
                                                  :on-reveal [(fn [game id]
                                                                (let [monsters (filter #(= (:type %) :monster) (vals (:characters game)))]
                                                                  (if (empty? monsters)
                                                                    game
                                                                    (reduce #(set-character %1 (increment-stat %2 :attack 2)) game monsters)
                                                                    )))]
                                                    }

                                                  :spike-trap
                                                  {
                                                    :type :event
                                                    :sub-type :trap
                                                    :name "Spike Trap"
                                                    :text "Caught! Take 1d4 damage per round for 2 rounds."
                                                    :on-reveal [(fn[game id]
                                                                  (let [trap-dmg {:dmg-dice-num 1
                                                                                  :dmg-dice-sides 4
                                                                                  :attack 0
                                                                                  :damage-type :physical
                                                                                  :crit 20
                                                                                  :crit-multiplier 1
                                                                                  :bonus-dmg 0
                                                                                  :name "Spike Trap"}]

                                                                    (apply-effect game
                                                                      (assoc (make-effect :damage :spike-trap 2 trap-dmg id)
                                                                        :target-type :hero :num-targets 1)
                                                                      (list (rand-nth (:heroes game))))))]
                                                    }
                                               }
                                       }
                      :murky-forests {
                                       :name "Murky Forests"
                                       :text "The dank forests around these lands are old and wise and have caused trouble
                                       for the not so careful but curious mapper willing to put his name on unexplored lands.
                                       Today, these forests are almost forgotten for the man, for the joy of many a foul creature,
                                       and, for the forest itself."
                                       :numbers {:clearing 4 :roots-and-snares 2 :thorns 1 :poisonous-mushroom 1 :crows 2}
                                       :cards {
                                              :clearing
                                                    {
                                                      :type :event
                                                      :sub-type :terrain
                                                      :name "Clearing"
                                                      :text "You pick up speed... Gain stamina."
                                                      :on-reveal [(fn[game id] (assoc game :stamina (+ (:stamina game) 3)))]
                                                      }
                                              :roots-and-snares
                                                    {
                                                      :type :event
                                                      :sub-type :terrain
                                                      :name "Roots and snares"
                                                      :text "Caught in a tangle. Loose stamina."
                                                      :on-reveal [(fn[game id] (assoc game :stamina (- (:stamina game) 1)))]
                                                      }
                                              :thorns
                                              {
                                                :type :event
                                                :sub-type :terrain
                                                :name "Thorns"
                                                :text "Thorns cut your skin."
                                                :on-reveal [(fn[game id]
                                                              (let [trap-dmg {:dmg-dice-num 1
                                                                              :dmg-dice-sides 2
                                                                              :attack 0
                                                                              :damage-type :thorns
                                                                              :crit 20
                                                                              :crit-multiplier 2
                                                                              :bonus-dmg 0
                                                                              :name "Thorns"}]
                                                                (apply-effect game
                                                                  (make-effect :damage :thorns 2 trap-dmg id)
                                                                  (pick-n-random 2 (:heroes game)))))]
                                                }
                                              :poisonous-mushroom
                                              {
                                                :type :event
                                                :sub-type :terrain
                                                :name "Poisonous Mushroom"
                                                :text "You step on something and the last thing you remember is the foul smell."
                                                :on-reveal [(fn[game id]
                                                              (apply-effect game (make-effect :stun :poisonous-mushroom 2 {} id)
                                                                (list (rand-nth (:heroes game)))))]
                                                }
                                              :crows
                                              {
                                                :type :event
                                                :sub-type :ominous-things
                                                :name "Crows"
                                                :text "You are being watched."
                                                :on-reveal [(fn[game id]
                                                              (apply-effect game (make-effect :debuff :crows 2 {:resistances {:physical 2}} id)
                                                                (:heroes game)))]
                                                }
                                              }}
                      :forest-beasts {
                                              :name "Forest Beasts"
                                              :text "The animals have grown restless lately, tread carefully."
                                              :numbers {:snake 4 :black-bear 1 :boar 2 :wolf 3}
                                              :cards {
                                                       :snake
                                                       {
                                                         :type :monster
                                                         :name "Snake"
                                                         :keywords #{:beast}
                                                         :dmg-dice-num 1
                                                         :dmg-dice-sides 2
                                                         :crit-multiplier 2
                                                         :crit 20
                                                         :damage-type :physical
                                                         :resistances {:physical 15 :magic 0 :fire 0 :poison 0 :thorns 0}
                                                         :attack 0
                                                         :hp 4
                                                         :max-hp 4
                                                         :bonus-dmg 0
                                                         :initiative 7
                                                         }

                                                       :wolf
                                                       {
                                                         :type :monster
                                                         :name "Wolf"
                                                         :keywords #{:beast}
                                                         :dmg-dice-num 1
                                                         :dmg-dice-sides 3
                                                         :crit-multiplier 2
                                                         :crit 20
                                                         :damage-type :physical
                                                         :resistances {:physical 12 :magic 0 :fire 0 :poison 0 :thorns 0}
                                                         :attack 0
                                                         :hp 5
                                                         :max-hp 5
                                                         :bonus-dmg 0
                                                         :initiative 2
                                                         }

                                                       :boar
                                                       {
                                                         :type :monster
                                                         :keywords #{:beast}
                                                         :name "Boar"
                                                         :dmg-dice-num 1
                                                         :dmg-dice-sides 4
                                                         :crit-multiplier 2
                                                         :crit 19
                                                         :damage-type :physical
                                                         :resistances {:physical 13 :magic 0 :fire 0 :poison 0 :thorns 0}
                                                         :attack 0
                                                         :hp 6
                                                         :max-hp 6
                                                         :bonus-dmg 0
                                                         :initiative 2
                                                         }
                                                       :black-bear
                                                       {
                                                         :type :monster
                                                         :keywords #{:beast}
                                                         :name "Black Bear"
                                                         :dmg-dice-num 1
                                                         :dmg-dice-sides 6
                                                         :crit-multiplier 2
                                                         :crit 20
                                                         :damage-type :physical
                                                         :resistances {:physical 15 :magic 0 :fire 0 :poison 0 :thorns 0}
                                                         :attack 2
                                                         :hp 8
                                                         :max-hp 8
                                                         :bonus-dmg 0
                                                         :initiative 2
                                                         }
                                                       }
                                       }
                      })
