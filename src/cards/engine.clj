(ns cards.engine
  (:import (org.lwjgl.opengl Display DisplayMode GL11 GL20 GL12 GL15
                                     ARBShaderObjects
                                     ARBBufferObject
                                     ARBVertexBufferObject
                                     EXTFramebufferObject)
           (org.lwjgl.util.glu GLU)
           (org.lwjgl.util Timer)
           (org.lwjgl.openal AL AL10)
           (org.lwjgl.input Mouse Keyboard)
           (org.lwjgl BufferUtils)
           (org.newdawn.slick.util ResourceLoader)
           (org.newdawn.slick AngelCodeFont Image)
           (org.newdawn.slick.opengl Texture TextureLoader)
           (java.nio ByteBuffer ByteOrder IntBuffer)
           [org.newdawn.slick.openal AudioLoader SoundStore])
  (:use [cards shaders])
 )

(def width 1920)
(def height 1080)
(def glob-shader (atom {}))
(def glob-sprites (atom {}))
(def glob-textures (atom {}))
(def glob-fonts (atom {}))
(def glob-music (atom {}))
(def glob-sound-effects (atom {}))
(def string-vbo-cache (atom {}))
;(def string-cache (atom ()))

;; Math
(defn exp [n x]
  (reduce * (repeat x n)))
(defn length[x1 y1 x2 y2]
  (Math/sqrt (+ (exp (- x2 x1) 2) (exp (- y2 y1) 2))))

(def id (atom 4))
(defn generate-id[]
  (swap! id inc)
  @id)


(defn set-update-state[game update]
  (assoc game :update-state (list update)))

(defn push-update-state[game update]
  (assoc game :update-state (conj (:update-state game) update)))

(defn pop-update-state[game]
  (assoc game :update-state (pop (:update-state game))))

(defn push-render-state[game render]
  (assoc game :render-state (conj (:render-state game) render)))
(defn set-render-state[game render]
  (assoc game :render-state (list render)))
(defn pop-render-state[game]
  (assoc game :render-state (pop (:render-state game))))


(defn mouse-to-world[pos]
  [(- (Mouse/getX) (first pos)) (- (- height (Mouse/getY)) (second pos))])

(defn move-sprite [sprite x y]
  (assoc sprite :posx x :posy y))

(defn sprite-center[sprite]
  [(+ (:posx sprite) (/ (:width sprite) 2)) (+ (:posy sprite) (/ (:height sprite) 2))])

(defn center-sprite-at[sprite pos]
  (move-sprite sprite
    (- (first pos) (/ (:width sprite) 2))
    (- (second pos) (/ (:height sprite) 2))))


(defn make-input-state[]
  {:mouse-down #{} :mouse-pressed #{} :mouse-released #{} :dx 0 :dy 0 :drag-start nil :mousex 0 :mousey 0
   :keys-down #{} :keys-pressed #{} :keys-released #{}})

(defn get-key-events[old-state]
  (loop [event (Keyboard/next) state (assoc old-state :keys-down #{} :keys-released #{})]
    (if event
      (let [evt-key-state (Keyboard/getEventKeyState)
            evt-key (Keyboard/getEventKey)
            next (Keyboard/next)]
        (if evt-key-state
          ;; key pressed
          (recur next (assoc state
                        :keys-pressed (conj (:keys-pressed state) evt-key)
                        :keys-pressed (conj (:keys-pressed state) evt-key)))
          ;; key released
          (recur next (assoc state
                        :keys-released (conj (:keys-released state) evt-key)
                        :keys-pressed (disj (:keys-pressed state) evt-key)
                        :keys-down (disj (:keys-down state) evt-key)))
          )
        )
      state
      )
    )
  )

(defn get-mouse-events[old-state]
  (loop [event (Mouse/next) state (assoc old-state :mouse-down #{} :mouse-released #{} :dx 0 :dy 0)]

    (if event

      (let [evt-btn-state (Mouse/getEventButtonState)
            evt-btn (Mouse/getEventButton)
            state (assoc state
                    :dx (+ (:dx state) (Mouse/getEventDX))
                    :dy (+ (:dy state) (Mouse/getEventDY)))
            next (Mouse/next)]

        ;; button event
        (if evt-btn-state
          ;; mouse pressed

          (recur next (assoc state
                        :mouse-down (conj (:mouse-down state) evt-btn)
                        :mouse-pressed (conj (:mouse-pressed state) evt-btn)))

          ;; mouse button released / no button state
          (recur next
            (if (> evt-btn -1)
              (assoc state
                :mouse-released (conj (:mouse-released state) evt-btn)
                :mouse-down (disj (:mouse-down state) evt-btn)
                :mouse-pressed (disj (:mouse-pressed state) evt-btn))
              state
              )
            )
          )
        )
      (assoc state :mousex (Mouse/getX) :mousey (Mouse/getY))
      )
    )
  )

(defn load-texture [tex-file]
  (TextureLoader/getTexture "PNG" (ResourceLoader/getResourceAsStream tex-file)))


(defn get-handles[buffers]
  (let [handles (BufferUtils/createIntBuffer (count buffers))]
    (ARBBufferObject/glGenBuffersARB handles)

    (doseq [i (range (count buffers))]
      (let [handle (.get handles i)
            buffer (.get buffers i)]
        (ARBBufferObject/glBindBufferARB ARBVertexBufferObject/GL_ARRAY_BUFFER_ARB handle)
        (ARBBufferObject/glBufferDataARB ARBVertexBufferObject/GL_ARRAY_BUFFER_ARB buffer ARBBufferObject/GL_STATIC_DRAW_ARB)
        (.put handles i handle))
      )
    (ARBBufferObject/glBindBufferARB ARBVertexBufferObject/GL_ARRAY_BUFFER_ARB 0)
    handles
    )

  )

;(defn create-texture-buffer[tex]
;  (let [t-buffer (BufferUtils/createFloatBuffer 12)]
;    (doto t-buffer
;      (.put (float 0)) (.put (float (. tex getHeight))) (.put (float 0))
;      (.put (float (. tex getWidth))) (.put (float (. tex getHeight))) (.put (float 0))
;      (.put (float (. tex getWidth))) (.put (float 0)) (.put (float 0))
;      (.put (float 0)) (.put (float 0)) (.put (float 0))
;      (.flip))
;  t-buffer
;  ))


(defn create-float-buffer [array]
  (let [v-buffer (BufferUtils/createFloatBuffer (count array))]
    (doseq [x (range (count array))]
      (. v-buffer put (float (nth array x)))
      )
    (.position v-buffer 0)
    v-buffer
    )
  )

(defn create-int-buffer [array]
  (let [v-buffer (BufferUtils/createIntBuffer (count array))]
    (doseq [x (range (count array))]
      (. v-buffer put (int (nth array x)))
      )
    (.position v-buffer 0)
    v-buffer
    )
  )

(defn create-color-buffer[r g b]
  (let [c-buffer (BufferUtils/createFloatBuffer 12)]
    (doto c-buffer
      (.put (float r)) (.put (float g)) (.put (float b))
      (.put (float r)) (.put (float g)) (.put (float b))
      (.put (float r)) (.put (float g)) (.put (float b))
      (.put (float r)) (.put (float g)) (.put (float b))
      (.flip))
    c-buffer
    )
  )

(defn make-gl-buffer[gl-target bufferdata]
  (let [buffer (BufferUtils/createIntBuffer 1)]
    (GL15/glGenBuffers buffer)
    (let [id (.get buffer 0)]
      (GL15/glBindBuffer gl-target id)
      (GL15/glBufferData gl-target bufferdata GL15/GL_DYNAMIC_DRAW)
      (GL15/glBindBuffer gl-target 0)
      id
      )
    )
  )

(defn create-array-buffer[array]
  (let [v-buffer (create-float-buffer array)
        i-buffer (create-int-buffer (vec (range (/ (count array) 1))))]
    ; [0 1 2  2 3 0
    ;  3 2 4  4 5 3
    ;  5 4 6  6 7 5]
    {
      :v-buffer v-buffer
      :v-handle (make-gl-buffer GL15/GL_ARRAY_BUFFER v-buffer)
      :i-buffer i-buffer
      :i-handle (make-gl-buffer GL15/GL_ELEMENT_ARRAY_BUFFER i-buffer)
      }
    )
  )

(defn create-texture-buffer[w h]
  (let [t-buffer (BufferUtils/createFloatBuffer 12)]
    (doto t-buffer
      (.put (float 0)) (.put (float h)) (.put (float 0))
      (.put (float w)) (.put (float h)) (.put (float 0))
      (.put (float w)) (.put (float 0)) (.put (float 0))
      (.put (float 0)) (.put (float 0)) (.put (float 0))
      (.flip))
    t-buffer
    ))
;
(defn create-vertex-buffer[w h]
  (let [v-buffer (BufferUtils/createFloatBuffer 12)]
    (doto v-buffer
      (.put (float 0)) (.put (float h)) (.put (float 0))
      (.put (float w)) (.put (float h)) (.put (float 0))
      (.put (float w)) (.put (float 0)) (.put (float 0))
      (.put (float 0)) (.put (float 0)) (.put (float 0))
      (.flip))
      v-buffer
    ))
(defn create-color-buffer[r g b]
  (let [c-buffer (BufferUtils/createFloatBuffer 12)]
    (doto c-buffer
      (.put (float r)) (.put (float g)) (.put (float b))
      (.put (float r)) (.put (float g)) (.put (float b))
      (.put (float r)) (.put (float g)) (.put (float b))
      (.put (float r)) (.put (float g)) (.put (float b))
      (.flip))
    c-buffer
    )
  )
;; get the total delta input state (mouse and keyboard)
(defn get-input-state[old-state]
;  (println old-state)
  (get-mouse-events (get-key-events old-state)))

(defn create-sprite [w h]
  {:width w :height h :posx 0 :posy 0})

(defn create-quad[w h]
  (let[buffer (create-vertex-buffer w h)
       c-buffer (create-color-buffer 0 1 0)
       handle (get-handles [buffer c-buffer])]
    (assoc (create-sprite w h) :v-buffer buffer :c-buffer c-buffer :handles handle)))

(defn draw-primitive[pr x y]
  (GL11/glDisable GL11/GL_TEXTURE_2D)

;  (GL11/glPushMatrix)
;  (GL11/glTranslatef x y 0)

  (let [
        v-buffer (:v-buffer pr)
        c-buffer (:c-buffer pr)
        handles (:handles pr)
        v-handle (.get handles 0)
        c-handle (.get handles 1)]

    (GL11/glEnableClientState GL11/GL_VERTEX_ARRAY)
    (GL11/glEnableClientState GL11/GL_COLOR_ARRAY)

    (ARBBufferObject/glBindBufferARB ARBVertexBufferObject/GL_ARRAY_BUFFER_ARB v-handle)
    (GL11/glVertexPointer 3 GL11/GL_FLOAT (bit-shift-left 3 2) (long 0))

    (ARBBufferObject/glBindBufferARB ARBVertexBufferObject/GL_ARRAY_BUFFER_ARB c-handle)
    (GL11/glColorPointer 3 GL11/GL_FLOAT (bit-shift-left 3 2) (long 0))

    (GL11/glDrawArrays GL11/GL_QUADS 0 4)

    (ARBBufferObject/glBindBufferARB ARBVertexBufferObject/GL_ARRAY_BUFFER_ARB 0)

    (GL11/glDisableClientState GL11/GL_VERTEX_ARRAY)
    (GL11/glDisableClientState GL11/GL_COLOR_ARRAY)
    )

;  (GL11/glPopMatrix)
  (GL11/glEnable GL11/GL_TEXTURE_2D)
  )

;(defn draw-textured-sprite[sprite]
;
;  (GL11/glLoadName (:id sprite))
;  (GL11/glPushMatrix)
;  (GL11/glTranslatef (:posx sprite) (:posy sprite) 0)
;
;  (GL11/glEnable GL11/GL_TEXTURE_2D)
;
;  (GL11/glBindTexture GL11/GL_TEXTURE_2D (.getTextureID (:tex sprite)))
;
;  (let [vbo (:vbo sprite)
;        handles (:handles vbo)
;        v-handle (.get handles 0)
;        t-handle (.get handles 1)]
;
;    (GL11/glEnableClientState GL11/GL_VERTEX_ARRAY)
;    (GL11/glEnableClientState GL11/GL_TEXTURE_COORD_ARRAY)
;
;    (ARBBufferObject/glBindBufferARB ARBVertexBufferObject/GL_ARRAY_BUFFER_ARB v-handle)
;    (GL11/glVertexPointer 3 GL11/GL_FLOAT (bit-shift-left 3 2) (long 0))
;
;    (ARBBufferObject/glBindBufferARB ARBVertexBufferObject/GL_ARRAY_BUFFER_ARB t-handle)
;    (GL11/glTexCoordPointer 3 GL11/GL_FLOAT (bit-shift-left 3 2) (long 0))
;
;    (GL11/glDrawArrays GL11/GL_QUADS 0 4)
;
;    (ARBBufferObject/glBindBufferARB ARBVertexBufferObject/GL_ARRAY_BUFFER_ARB 0)
;
;    (GL11/glDisableClientState GL11/GL_VERTEX_ARRAY)
;    (GL11/glDisableClientState GL11/GL_TEXTURE_COORD_ARRAY)
;    )
;  (GL11/glDisable GL11/GL_TEXTURE_2D)
;  (GL11/glPopMatrix)
;  )


(defn draw-fbo-tex[obj]
  (. org.newdawn.slick.Color/white bind)

  (GL11/glEnable GL11/GL_TEXTURE_2D)
  (EXTFramebufferObject/glBindFramebufferEXT EXTFramebufferObject/GL_FRAMEBUFFER_EXT 0)
  (GL11/glClear GL11/GL_COLOR_BUFFER_BIT)

  (GL11/glBindTexture GL11/GL_TEXTURE_2D (:tex-id obj))
  (GL11/glViewport 0 0 (:width obj) (:height obj))
  (GL11/glLoadName (:id obj))
  (GL11/glPushMatrix)
  (GL11/glTranslatef (:posx obj) (:posy obj) 0)
  (let [
         v-handle (:v-handle obj)
         t-handle (:t-handle obj)]

    (GL11/glEnableClientState GL11/GL_VERTEX_ARRAY)
    (GL11/glEnableClientState GL11/GL_TEXTURE_COORD_ARRAY)

    (ARBBufferObject/glBindBufferARB ARBVertexBufferObject/GL_ARRAY_BUFFER_ARB v-handle)
    (GL11/glVertexPointer 3 GL11/GL_FLOAT (bit-shift-left 3 2) (long 0))

    (ARBBufferObject/glBindBufferARB ARBVertexBufferObject/GL_ARRAY_BUFFER_ARB t-handle)
    (GL11/glTexCoordPointer 3 GL11/GL_FLOAT (bit-shift-left 3 2) (long 0))

    (GL11/glDrawArrays GL11/GL_QUADS 0 4)

    (ARBBufferObject/glBindBufferARB ARBVertexBufferObject/GL_ARRAY_BUFFER_ARB 0)

    (GL11/glDisableClientState GL11/GL_VERTEX_ARRAY)
    (GL11/glDisableClientState GL11/GL_TEXTURE_COORD_ARRAY)
    )
  (GL11/glDisable GL11/GL_TEXTURE_2D)
  (GL11/glPopMatrix)
  )

(defn get-ortographic-projection-matrix [r l t b n f]
  (let [proj   [(/ 2.0 (- r l)) 0 0 0
                0 (/ 2.0 (- t b)) 0 0
                0 0 (/ -2.0 (- f n)) 0
                (* -1.0 (/ (+ r l) (- r l))) (* -1.0 (/ (+ t b) (- t b))) (* -1.0 (/ (+ f n) (- f n))) 1]
        ]
    (create-float-buffer proj)
    )
  )
(def ftest (get-ortographic-projection-matrix width 0 0 height 0 1))

(defn draw-tex[sprite text & {:keys [color-add opacity]
                             :or {color-add [0.0 0.0 0.0 0.0] opacity 1.0}}]
  (GL11/glLoadName (:id sprite))
  (load-shader (:tex @glob-shader))

  (let [shader (:tex @glob-shader)
        v-handle (:v-handle (:buffer sprite))
        t-handle (:v-handle text)
        vi-buffer (:v-buffer (:buffer sprite))
        vi-handle (:i-handle (:buffer sprite))

        attid (GL20/glGetAttribLocation shader "coord2d")
        texattid (GL20/glGetAttribLocation shader "texcoord")
        pos (GL20/glGetUniformLocation shader "pos")
        proj (GL20/glGetUniformLocation shader "proj")
        tex (GL20/glGetUniformLocation shader "tex")
        cadd (GL20/glGetUniformLocation shader "color_add")]

    (GL20/glUniform2f pos (:posx sprite) (:posy sprite))
    (GL20/glUniform4f cadd  (nth color-add 0) (nth color-add 1) (nth color-add 2) (nth color-add 3))
    (GL20/glUniformMatrix4 proj false ftest)
    (GL20/glUniform1i tex 0)

    (GL11/glEnable GL11/GL_TEXTURE_2D)

    (GL11/glBindTexture GL11/GL_TEXTURE_2D (:id text))

    (GL15/glBindBuffer GL15/GL_ARRAY_BUFFER v-handle)
    (GL20/glVertexAttribPointer attid 2 GL11/GL_FLOAT false 0 0)
    (GL20/glEnableVertexAttribArray attid)

    (GL15/glBindBuffer GL15/GL_ARRAY_BUFFER t-handle)
    (GL20/glVertexAttribPointer texattid 2 GL11/GL_FLOAT false 0 0)
    (GL20/glEnableVertexAttribArray texattid)

    (GL15/glBindBuffer GL15/GL_ELEMENT_ARRAY_BUFFER vi-handle)
    (GL12/glDrawRangeElements GL11/GL_QUADS 0 (- (.capacity vi-buffer) 1) (.capacity vi-buffer) GL11/GL_UNSIGNED_INT 0)
    (GL20/glDisableVertexAttribArray attid)
    (GL20/glDisableVertexAttribArray texattid)
    (GL15/glBindBuffer GL15/GL_ARRAY_BUFFER 0)
    (GL15/glBindBuffer GL15/GL_ELEMENT_ARRAY_BUFFER 0)
    )
  (release-shader)

  )

(defn draw-interleaved[sprite tex-id x y & {:keys [r g b a] :or {r 0 g 0 b 0 a 1}}]

  (load-shader (:text @glob-shader))

  (let [shader (:text @glob-shader)
        v-handle (:v-handle sprite)
        vi-buffer (:i-buffer sprite)
        vi-handle (:i-handle sprite)
        attid (GL20/glGetAttribLocation shader "coord2d")
        pos (GL20/glGetUniformLocation shader "pos")
        proj (GL20/glGetUniformLocation shader "proj")
        tex (GL20/glGetUniformLocation shader "tex")
        col (GL20/glGetUniformLocation shader "color")]

    (GL20/glUniform2f pos x y)
    (GL20/glUniform4f col r g b a)
    (GL20/glUniformMatrix4 proj false ftest)
    (GL20/glUniform1i tex 0)

    (GL11/glEnable GL11/GL_TEXTURE_2D)

    (GL11/glBindTexture GL11/GL_TEXTURE_2D tex-id)

    (GL15/glBindBuffer GL15/GL_ARRAY_BUFFER v-handle)
    (GL20/glVertexAttribPointer attid 4 GL11/GL_FLOAT false 0 0)
    (GL20/glEnableVertexAttribArray attid)

    (GL15/glBindBuffer GL15/GL_ELEMENT_ARRAY_BUFFER vi-handle)
    (GL12/glDrawRangeElements GL11/GL_QUADS 0 (- (.capacity vi-buffer) 1) (.capacity vi-buffer) GL11/GL_UNSIGNED_INT 0)
    (GL20/glDisableVertexAttribArray attid)
    (GL15/glBindBuffer GL15/GL_ARRAY_BUFFER 0)
    (GL15/glBindBuffer GL15/GL_ELEMENT_ARRAY_BUFFER 0)
    )
  (release-shader)

  )

;(defn draw-tex[sprite tex & {:keys [color-add opacity]
;                             :or {color-add [0.0 0.0 0.0 0.0] opacity 1.0}}]
;  (. org.newdawn.slick.Color/white bind)
;
;  (GL11/glEnable GL11/GL_TEXTURE_2D)
;  (GL11/glBindTexture GL11/GL_TEXTURE_2D (.getTextureID (:tex tex)))
;
;  (GL11/glLoadName (:id sprite))
;  (GL11/glPushMatrix)
;  (GL11/glTranslatef (:posx sprite) (:posy sprite) 0)
;
;  (load-shader (:tex @glob-shader))
;  (let [cadd (get-uniform-location (:tex @glob-shader) "color_add")
;        v-handle (:v-handle sprite)
;        t-handle (:t-handle tex)]
;
;    (GL20/glUniform4f cadd  (nth color-add 0) (nth color-add 1) (nth color-add 2) (nth color-add 3))
;;    (GL20/glUniform4f cadd  -0.5 -0.5 -0.5 0)
;
;    (GL11/glEnableClientState GL11/GL_VERTEX_ARRAY)
;    (GL11/glEnableClientState GL11/GL_TEXTURE_COORD_ARRAY)
;
;    (ARBBufferObject/glBindBufferARB ARBVertexBufferObject/GL_ARRAY_BUFFER_ARB v-handle)
;    (GL11/glVertexPointer 3 GL11/GL_FLOAT (bit-shift-left 3 2) (long 0))
;
;    (ARBBufferObject/glBindBufferARB ARBVertexBufferObject/GL_ARRAY_BUFFER_ARB t-handle)
;    (GL11/glTexCoordPointer 3 GL11/GL_FLOAT (bit-shift-left 3 2) (long 0))
;
;    (GL11/glDrawArrays GL11/GL_QUADS 0 4)
;
;    (ARBBufferObject/glBindBufferARB ARBVertexBufferObject/GL_ARRAY_BUFFER_ARB 0)
;
;    (GL11/glDisableClientState GL11/GL_VERTEX_ARRAY)
;    (GL11/glDisableClientState GL11/GL_TEXTURE_COORD_ARRAY)
;    )
;  (release-shader)
;
;  (GL11/glDisable GL11/GL_TEXTURE_2D)
;  (GL11/glPopMatrix)
;;    (. (:tex tex))
;  )

(def colours {:black org.newdawn.slick.Color/black
              :white org.newdawn.slick.Color/white
              :blue org.newdawn.slick.Color/blue
              :red org.newdawn.slick.Color/red
              :green org.newdawn.slick.Color/green})




(defn draw-quad[x y width height & {:keys [r g b a id] :or {r 1 g 0 b 0 a 1 id 0}}]

  (GL11/glDisable GL11/GL_TEXTURE_2D)

  (load-shader (:quad @glob-shader))
  (let[shader (:quad @glob-shader)
       sprite (:quad @glob-sprites)
       v-handle (:v-handle (:buffer sprite))
       vi-buffer (:i-buffer (:buffer sprite))
       vi-handle (:i-handle (:buffer sprite))
       w (GL20/glGetUniformLocation shader "width")
       h (GL20/glGetUniformLocation shader "height")
       c (GL20/glGetUniformLocation shader "color")
       pos (GL20/glGetUniformLocation shader "pos")
       proj (GL20/glGetUniformLocation shader "proj")
       attid (GL20/glGetAttribLocation shader "coord2d")]

    (GL20/glUniform1f w width)
    (GL20/glUniform1f h height)
    (GL20/glUniform4f c r g b a)
    (GL20/glUniform2f pos x y)
    (GL20/glUniformMatrix4 proj false ftest)

    (GL15/glBindBuffer GL15/GL_ARRAY_BUFFER v-handle)
    (GL20/glVertexAttribPointer attid 2 GL11/GL_FLOAT false 0 0)
    (GL20/glEnableVertexAttribArray attid)

    (GL15/glBindBuffer GL15/GL_ELEMENT_ARRAY_BUFFER vi-handle)
    (GL12/glDrawRangeElements GL11/GL_QUADS 0 (- (.capacity vi-buffer) 1) (.capacity vi-buffer) GL11/GL_UNSIGNED_INT 0)

    (GL20/glDisableVertexAttribArray attid)
    (GL15/glBindBuffer GL15/GL_ARRAY_BUFFER 0)
    (GL15/glBindBuffer GL15/GL_ELEMENT_ARRAY_BUFFER 0)

    )
;  (GL11/glLoadName id)
;  (draw-primitive  x y)
  (release-shader)
  )

;(defn draw-test[]
;  (GL11/glEnable GL11/GL_TEXTURE_2D)
;  (GL11/glBindTexture GL11/GL_TEXTURE_2D (.getTextureID (:tex (:test-card @glob-textures))))
;
;;  (GL11/glLoadName (:id sprite))
;;  (GL11/glPushMatrix)
;;  (GL11/glTranslatef (:posx sprite) (:posy sprite) 0)
;
;  (load-shader (:test-shader @glob-shader))
;  (let [tex (GL20/glGetUniformLocation (:test-shader @glob-shader) "tex")
;        v-handle (:v-handle (:test-sprite @glob-sprites))
;        t-handle (:t-handle (:test-card @glob-textures))]
;
;;    (GL20/glUniform4f cadd  (nth color-add 0) (nth color-add 1) (nth color-add 2) (nth color-add 3))
;    ;    (GL20/glUniform4f cadd  -0.5 -0.5 -0.5 0)
;    (GL20/glUniform1i tex 0)
;    (GL11/glEnableClientState GL11/GL_VERTEX_ARRAY)
;    (GL11/glEnableClientState GL11/GL_TEXTURE_COORD_ARRAY)
;
;    (ARBBufferObject/glBindBufferARB ARBVertexBufferObject/GL_ARRAY_BUFFER_ARB v-handle)
;    (GL11/glVertexPointer 3 GL11/GL_FLOAT (bit-shift-left 3 2) (long 0))
;
;    (ARBBufferObject/glBindBufferARB ARBVertexBufferObject/GL_ARRAY_BUFFER_ARB t-handle)
;    (GL11/glTexCoordPointer 3 GL11/GL_FLOAT (bit-shift-left 3 2) (long 0))
;
;    (GL11/glDrawArrays GL11/GL_QUADS 0 4)
;
;    (ARBBufferObject/glBindBufferARB ARBVertexBufferObject/GL_ARRAY_BUFFER_ARB 0)
;
;    (GL11/glDisableClientState GL11/GL_VERTEX_ARRAY)
;    (GL11/glDisableClientState GL11/GL_TEXTURE_COORD_ARRAY)
;    )
;  (release-shader)
;
;  (GL11/glDisable GL11/GL_TEXTURE_2D)
;;  (GL11/glPopMatrix)
;  ;    (. (:tex tex))
;
;  )


(defn draw-line[ox oy tx ty width & {:keys [r g b a] :or {r 1 g 0 b 0 a 1}}]
  (let [length (length ox oy tx ty)
        cos (/ (- tx ox) length)
        angle (Math/acos cos)
        corr-angle (if (< (- ty oy) 0) (- angle (* 2 angle)) angle)
        quad (:quad @glob-sprites)]

    (load-shader (:line @glob-shader))
    (let[w (GL20/glGetUniformLocation (:line @glob-shader) "pos")
         r (GL20/glGetUniformLocation (:line @glob-shader) "angle")]
      (GL20/glUniform1f r corr-angle)
      (GL20/glUniform4f w length width 1 1)
      (draw-primitive quad ox oy)
      (release-shader))))

;(defn create-textured-sprite[tex-file w h]
;  (let [tex (load-texture tex-file)
;        v-buffer (create-vertex-buffer w h)
;        t-buffer (create-texture-buffer tex)
;        handles (get-handles [v-buffer t-buffer])]
;    (assoc (create-sprite w h)
;      :vbo {:v-buffer v-buffer :t-buffer t-buffer :handles handles}
;      :tex tex :image-width (. tex getWidth)
;      :image-height (. tex getHeight)
;      :id 0)))

;(defn create-base-sprite[w h]
;  (let [spr (create-sprite w h)
;        v-buffer (create-vertex-buffer w h)
;        handles (get-handles [v-buffer])]
;    (assoc spr :buffer v-buffer :v-handle (. handles get 0))
;    )
;  )

;[0.0  0.0 (+ 0.0 (/ x width)) (+ 0.0 (/ y height))
; w 0.0 (+ 0.0 (/ x width) (/ w width)) (+ 0.0 (/ y height))
; w h (+ 0.0 (/ x width) (/ w width)) (+ 0.0 (/ y height) (/ h height))
; 0.0 h (+ 0.0 (/ x width)) (+ 0.0 (/ y height) (/ h height))]

(defn create-base-sprite[w h]
  (let [spr (create-sprite w h)
        v-buffer (create-array-buffer [0 0
                                       w 0
                                       w h
                                       0 h])]
    (assoc spr :buffer v-buffer :id 0)
    )
  )

(defn create-texture[file]
  (let [tex (load-texture file)
        t-buffer (create-array-buffer
                   [0 0
                    (. tex getWidth) 0
                    (. tex getWidth) (. tex getHeight)
                    0 (. tex getHeight)] )
;        handles (get-handles [t-buffer])
        ]
    (assoc t-buffer :tex tex :id (.getTextureID tex) :image-width (. tex getWidth) :image-height (. tex getHeight))
;     :buffer t-buffer :t-handle (. handles get 0)

    ))

;(defn draw-disc [x y]
;  (GL11/glBindTexture GL11/GL_TEXTURE_2D (.getTextureID (:tex (:tex-square @glob-sprites))))
;  (load-shader (:disc @glob-shader))
;  (let[t (GL20/glGetUniformLocation (:disc @glob-shader) "tex")
;       spr (center-sprite-at (:tex-square @glob-sprites) [x y])]
;    (GL20/glUniform1i t 0)
;    (draw-textured-sprite spr)
;    (release-shader)))

(defn init-framebuffers[w h]
  (let [fbo-id (EXTFramebufferObject/glGenFramebuffersEXT)
        col-id (GL11/glGenTextures)]
    (EXTFramebufferObject/glBindFramebufferEXT EXTFramebufferObject/GL_FRAMEBUFFER_EXT fbo-id)
    (GL11/glEnable GL11/GL_TEXTURE_2D)
    (GL11/glBindTexture GL11/GL_TEXTURE_2D col-id)
    (GL11/glTexParameterf GL11/GL_TEXTURE_2D GL11/GL_TEXTURE_MIN_FILTER GL11/GL_LINEAR)
    (GL11/glTexImage2D GL11/GL_TEXTURE_2D  0 GL11/GL_RGBA8 w h 0 GL11/GL_RGBA GL11/GL_INT (cast java.nio.ByteBuffer nil))
    (EXTFramebufferObject/glFramebufferTexture2DEXT EXTFramebufferObject/GL_FRAMEBUFFER_EXT EXTFramebufferObject/GL_COLOR_ATTACHMENT0_EXT GL11/GL_TEXTURE_2D col-id 0)

    (EXTFramebufferObject/glBindFramebufferEXT EXTFramebufferObject/GL_FRAMEBUFFER_EXT 0)
    (GL11/glDisable GL11/GL_TEXTURE_2D)
    [fbo-id col-id]
    )
  )

(defn init-fbo-sprite-tex [width height]
  (let [fbo (init-framebuffers width height)
         fbo-id (first fbo)
         col-id (second fbo)
         t-buffer (create-texture-buffer width height)
         v-buffer (create-vertex-buffer width height)
         handles (get-handles [t-buffer v-buffer])
         ]
    (assoc
      (create-base-sprite width height)
      :tex-id col-id :fbo-id fbo-id :t-handle (. handles get 0) :v-handle (. handles get 1)
      )
    )
  )

(defn start-render-to-fbo [fbo]
  (GL11/glViewport 0 0 512 512)
  (GL11/glBindTexture GL11/GL_TEXTURE_2D 0)
  (EXTFramebufferObject/glBindFramebufferEXT EXTFramebufferObject/GL_FRAMEBUFFER_EXT (:fbo-id fbo))
  )

(defn start-render-texture [col-id w h]
  (GL11/glEnable GL11/GL_TEXTURE_2D)
  (EXTFramebufferObject/glBindFramebufferEXT EXTFramebufferObject/GL_FRAMEBUFFER_EXT 0)
  (GL11/glClear GL11/GL_COLOR_BUFFER_BIT)
  (GL11/glBindTexture GL11/GL_TEXTURE_2D col-id)
  (GL11/glViewport 0 0 512 512)
;  (GL11/glDisable GL11/GL_TEXTURE_2D)

  )

(defn draw-blur [game]
;  (load-shader (:test-hor @glob-shader))
  (start-render-to-fbo (:fbo game))
  (draw-tex (assoc (:card-sprite @glob-sprites) :posx 0 :posy 0 :id 0) (:event-card @glob-textures))

;  (start-render-texture (:tex-id (:fbo game)) 100 100)
  (draw-fbo-tex (assoc (:fbo game) :id 0 :posx 0 :posy 0))

;  (release-shader)
  )

(defn play-music [track-key & {:keys [loop pitch gain] :or {loop true pitch 1.0 gain 1.0}}]
  (.playAsMusic (track-key @glob-music) pitch gain loop))

(defn play-sound [track-key & {:keys [loop pitch gain] :or {loop true pitch 1.0 gain 1.0}}]
  (.playAsSoundEffect (track-key @glob-sound-effects) pitch gain loop))

;;; game loop
(def lwjgl-timer (Timer.))
(defn get-time[]
  (. lwjgl-timer getTime))
(defn get-tick-count[]
  (Timer/tick)
  (* (. lwjgl-timer getTime) 1000.0))

(def max-frame-skip 5)
(def ticks-per-second 60)
(def skip-ticks (/ 1000.0 ticks-per-second))
(def t (atom {:render-time 0.0 :fps 0.0 :tick 0.0 :update-time 0.0}))



;AL10.alSourceStop(sourceID);
;AL10.alSourcei(sourceID, AL10.AL_BUFFER, 0);
(defn destroy-game[]
;  (println "Stopping sources: " (. (SoundStore/get) getSourceCount))
;  (doseq [x (range (. (SoundStore/get) getSourceCount))]
;    (let [source (. (SoundStore/get) getSource x)]
;      (AL10/alSourceStop source)
;      (AL10/alSourcei source AL10/AL_BUFFER  0)
;      )
;    )

  (Display/destroy)
  )

(defn load-streaming-audio[format path]
  (AudioLoader/getStreamingAudio format (ResourceLoader/getResource path)))

(defn load-audio[format path]
  (AudioLoader/getAudio format (ResourceLoader/getResourceAsStream path)))

(defn scale [x y]
  (GL11/glScalef x y 1)
  )

(def frame-time (atom 0))
(def delta (atom 0))
(def frames (atom 0))
(def fps (atom ""))
(def fps-string (atom ""))
(defn update-times []
  (let [time (get-time)]
    (reset! fps-string (str "fps: " @fps ));" \ttime: " (- time @frame-time)))
    (if (>= @delta 1)
      (do
        (reset! fps (str @frames))
        (reset! frames 0)
        (reset! delta 0)
        )
      (do
        (swap! frames inc)
        )
      )

    (swap! delta + (- time @frame-time))

    (reset! frame-time time)
    )
  )

(defn run[game]
  (loop [loop-tick (get-tick-count)
         game game]

    (cond (Display/isCloseRequested)
      (do (destroy-game) (println "Exiting...") nil)
      (:exit game)
      (do (destroy-game) (println "Exiting...") nil)
    :else
      (let [elapsed
            (loop [game game
                   next-game-tick loop-tick
                   loops 0]
              (if (and (> (get-tick-count) next-game-tick) (< loops max-frame-skip))
                (do
;                  (-> (org.newdawn.slick.openal.SoundStore/get)
;                    (.poll 0) )
                  (recur ((:on-update game) (assoc game :input-state (get-input-state (:input-state game)))) (+ next-game-tick skip-ticks) (inc loops)))
                [game next-game-tick]))]


        (do
        ((:on-render game) (first elapsed) (/ (- (+ (get-tick-count) skip-ticks) (second elapsed)) skip-ticks ))
          (update-times)
          (Display/setTitle @fps-string)
          )

        (Display/update)

        (recur (second elapsed) (first elapsed))))))

;; TODO: this is slow
;(defn GL-select[game render & {:keys [x y w h] :or {x (double (Mouse/getX)) y (double (Mouse/getY)) w 1 h 1}}]
;  (let [sel-buffer (-> (ByteBuffer/allocateDirect 1024) (. order (ByteOrder/nativeOrder)) (. asIntBuffer))
;        vp-buffer  (-> (ByteBuffer/allocateDirect 64) (. order (ByteOrder/nativeOrder)) (. asIntBuffer))
;        buffer (int-array 256)
;        viewport (int-array 4)]
;    (GL11/glSelectBuffer sel-buffer)
;    (GL11/glGetInteger GL11/GL_VIEWPORT vp-buffer)
;    (. vp-buffer get viewport)
;    (GL11/glRenderMode GL11/GL_SELECT)
;    (GL11/glInitNames)
;    (GL11/glPushName 0)
;    (GL11/glMatrixMode GL11/GL_PROJECTION)
;    (GL11/glPushMatrix)
;    (GL11/glLoadIdentity)
;    (GLU/gluPickMatrix x y w h (IntBuffer/wrap viewport))
;;    (GL11/glOrtho 0 width height 0.0 0.0 1.0)
;    (GL11/glViewport 0 0 (Display/getWidth) (Display/getHeight))
;    (GL11/glMatrixMode GL11/GL_MODELVIEW)
;    (render game 0.0)
;    (GL11/glMatrixMode GL11/GL_PROJECTION)
;    (GL11/glPopMatrix)
;    (GL11/glMatrixMode GL11/GL_MODELVIEW)
;    (let [hits (GL11/glRenderMode GL11/GL_RENDER)]
;      (. sel-buffer get buffer)
;      (if (> hits 0)
;        (nth buffer (+ (* (dec hits) 4) 3))
;      -1)))
;
;  )

;; entities should be ordered top -> down
(defn mouse-select[entities & {:keys [x y] :or {x (double (Mouse/getX)) y (- height (double (Mouse/getY)))}}]

  (some #(if (and
               (>= x (:posx %)) (<= x (+ (:posx %) (:width %)))
               (>= y (:posy %)) (<= y (+ (:posy %) (:height %))))
           (:id %)
           ) entities)

  )

(defn init-gl[]
  (reset! string-vbo-cache {})
  (Display/setDisplayMode (DisplayMode. width height))
  (Display/setTitle "UNNAMED CARD GAME")
  (Display/setFullscreen false)
  (doseq [mode (Display/getAvailableDisplayModes)]
    (when (and (= (. mode getWidth) width) (= (. mode getHeight) height))
      (Display/setDisplayMode mode)
      )
;;    (println (. mode getWidth) "x" (. mode getHeight)
;;      "bpp" (. mode getBitsPerPixel)
;;      "freq" (. mode getFrequency) "Hz")
    )
  (Display/create)

  (GL11/glClearColor 0 0 0 0)
  (GL11/glViewport 0 0 width height)
;
;  (GL11/glClearColor 0.0 0.0 0.0 0.0)
;
;  (GL11/glDisable GL11/GL_DEPTH_TEST)
;  (GL11/glDisable GL11/GL_LIGHTING)
;
  (GL11/glEnable GL11/GL_TEXTURE_2D)
  (GL11/glEnable GL11/GL_BLEND)
  (GL11/glBlendFunc GL11/GL_SRC_ALPHA GL11/GL_ONE_MINUS_SRC_ALPHA)
;
;  (GL11/glMatrixMode GL11/GL_PROJECTION)
;  (GLU/gluOrtho2D 0.0 width height 0)
;  (GL11/glMatrixMode GL11/GL_MODELVIEW)
;  (GL11/glClear GL11/GL_COLOR_BUFFER_BIT)

  (swap! glob-sprites assoc :quad (create-base-sprite 1 1))
  (swap! glob-shader assoc :quad (create-shader-program "quad"))
;  (swap! glob-shader assoc :tex (create-shader-program "tex"))
  (swap! glob-shader assoc :tex (create-shader-program "test"))
  (swap! glob-shader assoc :text (create-shader-program "text"))


  )




(defn update-buffer[buffer-data]
  ;  (GL15/glBindBuffer GL15/GL_ARRAY_BUFFER buffer-id)
  ;  (GL15/glBufferData GL15/GL_ARRAY_BUFFER 1024 GL15/GL_DYNAMIC_DRAW)
  (GL15/glBufferSubData GL15/GL_ARRAY_BUFFER 0 buffer-data)
  ;  (GL15/glBindBuffer GL15/GL_ARRAY_BUFFER 0)
  )
;

;(def b (for [x (range 128)] (byte x)))
;(defn rb [n] (byte-array (for [x (range n)] (rand-nth b))))
;(let [a (reduce #(assoc %1 (String. (rb 1000) "ASCII") %2) {} (range 1000))])
(defn translate[vxuvarr x y]
  (-> vxuvarr
    (assoc 0   (+ x (vxuvarr 0)))
    (assoc 4   (+ x (vxuvarr 4)))
    (assoc 8   (+ x (vxuvarr 8)))
    (assoc 12  (+ x (vxuvarr 12)))
    (assoc 1   (+ y (vxuvarr 1)))
    (assoc 5   (+ y (vxuvarr 5)))
    (assoc 9   (+ y (vxuvarr 9)))
    (assoc 13  (+ y (vxuvarr 13)))
    )
  )

(defn get-string-vbo[font string]
  (let [lines (clojure.string/split-lines string)
        line-vbos
        (reduce (fn [vbolist line]
              (let [str (.getBytes (lines line))
                    string-vbo
                    (loop [b str   offset 0   all []   line-width 0]
                                 (if (empty? b)
                                   {:buffer (flatten all) :width line-width}
                                   (let [char ((:char-map font) (first b))]
                                     (if (nil? char)
                                       (recur (rest b) offset all line-width)
                                       (let [vxuvarr (:vxuv char)
                                             updated (translate vxuvarr
                                                       (+ offset (:xoffset char))
                                                       (+ (* line (:line-height font)) (:yoffset char)))
                                             offset (+ offset  (:xadvance char))
                                             width (+ (:w char) offset)]
                                         (recur (rest b) offset (conj all updated) width))))))]
                (conj vbolist string-vbo))
              ) [] (range (count lines)))

        string-vbo (reduce #(assoc %1
                              :buffer (concat (:buffer %1) (:buffer %2))
                              :height (+ (:height %1) (:line-height font))
                              :width (if (> (:width %1) (:width %2)) (:width %1) (:width %2))
                              ) {:buffer [] :height 0 :width 0} line-vbos)
        v-buffer (create-float-buffer (:buffer string-vbo))
        vboid (make-gl-buffer GL15/GL_ARRAY_BUFFER v-buffer)
        i-buffer (create-int-buffer (vec (range (/ (count (:buffer string-vbo)) 4))))
        iid (make-gl-buffer GL15/GL_ELEMENT_ARRAY_BUFFER i-buffer)
        string-vbo {:v-buffer v-buffer :i-buffer i-buffer :v-handle vboid :i-handle iid
                    :height (:height string-vbo) :width (:width string-vbo) :id (:id (:texture font))}]
        string-vbo
    )

  )
(defn get-cached-string-vbo [font string]
  (if (contains? (set (keys @string-vbo-cache)) string)
    (@string-vbo-cache string)
    (let [str-vbo (get-string-vbo font string)]
      (swap! string-vbo-cache assoc string str-vbo)
      str-vbo
      )
    )
  )
(defn get-string-height[font str]
  (let [vbo (get-cached-string-vbo font str)]
    (:height vbo)))

(defn get-string-width[font str]
  (let [vbo (get-cached-string-vbo font str)]
    (:width vbo)))

(defn draw-static-string[str-vbo x y]
  (draw-interleaved str-vbo (:id str-vbo) x y))

(defn draw-cached-string [font string x y & {:keys [r g b a] :or {r 0 g 0 b 0 a 1}}]
  (let [str-vbo (get-cached-string-vbo font string)]
    (draw-interleaved str-vbo (:id  (:texture font)) x y :r r :g g :b b :a a)))


(defn draw-string[font string x y & {:keys [r g b a] :or {r 0 g 0 b 0 a 1}}]
  (draw-cached-string font string x y :r r :g g :b b :a a)
  ;
  ;;  (. org.newdawn.slick.Color/white bind)
  ;  (GL11/glPushMatrix)
  ;  (GL11/glTranslatef x y 0)
  ;  (GL11/glEnable GL11/GL_TEXTURE_2D)
  ;  (GL11/glBindTexture GL11/GL_TEXTURE_2D  0)
  ;  (GL11/glBindTexture GL11/GL_TEXTURE_2D  (. (:tex (:tex font))  getTextureID))
  ;  (. (:ac font) drawString 0 0 string (colour colours))
  ;;  (. org.newdawn.slick.Color/white bind)
  ;  (GL11/glDisable GL11/GL_TEXTURE_2D)
  ;  (GL11/glPopMatrix)
  )

(defn draw-centered-string[font string x y]
  (let [w (get-string-width font string)
        m (/ w 2.0)]
    (draw-string font string (- x m) y)
    )
  )
(require '[clojure.xml :as xml])
(defn load-font-xml[xml img]

  (let [font (xml/parse xml)
        content (:content font)
        scales (select-keys (:attrs (first (filter #(= (:tag %) :common) content))) [:scaleW :scaleH :lineHeight])
        width (double (read-string (:scaleW scales)))
        height (double (read-string (:scaleH scales)))
        line-height (double (read-string (:lineHeight scales)))
        chs (first (filter #(= (:tag %) :chars) content))
        char-map (reduce (fn[char-map ch]
                           (let [attrs (:attrs ch)
                                 x (double (read-string (:x attrs)))
                                 y (double (read-string (:y attrs)))
                                 w (double (read-string (:width attrs)))
                                 h (double (read-string (:height attrs)))
                                 ]
                             (assoc char-map (read-string (:id attrs))
                               {:x x :y y :w w :h h
                                :vxuv
                                [0.0  0.0 (+ 0.0 (/ x width)) (+ 0.0 (/ y height))
                                 w 0.0 (+ 0.0 (/ x width) (/ w width)) (+ 0.0 (/ y height))
                                 w h (+ 0.0 (/ x width) (/ w width)) (+ 0.0 (/ y height) (/ h height))
                                 0.0 h (+ 0.0 (/ x width)) (+ 0.0 (/ y height) (/ h height))]
                                :xoffset (read-string (:xoffset attrs))
                                :yoffset (read-string (:yoffset attrs))
                                :xadvance (read-string (:xadvance attrs))}))
                           ) {} (:content chs))]
;    (println char-map)
    {:sizex width :sizey height :line-height line-height :char-map char-map :texture (create-texture img)
;     :string-uv-buffer (create-array-buffer (vec (repeat 1024 0)))
;     :string-vx-buffer (create-array-buffer (vec (repeat 1024 0)))
;     :string-pos-buffer (create-array-buffer (vec (repeat 1024 0)))
     }))
;; TODO: font rendering is slow, especially with caching set to true.
(defn create-fonts[]
  (reset! glob-fonts
    {
      :celtic-12 (load-font-xml "resources/fonts/test.fnt" "resources/fonts/test_0.png")
      :celtic-42 (load-font-xml "resources/fonts/test_large.fnt" "resources/fonts/test_large_0.png")
      ;      {:tex (create-texture "resources/fonts/celtic_12_0.png");(:tex (create-textured-sprite "resources/fonts/celtic_12_0.png" 128 128))
      ;                  :ac (AngelCodeFont. "resources/fonts/celtic_12.fnt" "resources/fonts/celtic_12_0.png" true)}
      ;      :mmed-12 {:tex (create-texture "resources/fonts/mmed_12_0.png");(:tex (create-textured-sprite "resources/fonts/celtic_12_0.png" 128 128))
      ;                  :ac (AngelCodeFont. "resources/fonts/mmed_12.fnt" "resources/fonts/mmed_12_0.png" true)}
      ;    :celtic-18 {:tex (create-texture "resources/fonts/celtic_18_0.png");(:tex (create-textured-sprite "resources/fonts/celtic_18_0.png" 256 256))
      ;                :ac (AngelCodeFont. "resources/fonts/celtic_18.fnt" "resources/fonts/celtic_18_0.png" true)}

      }
    )
  )
;; converts colour from range 0 - 255 to 0 - 1
(defn clamp-color[r g b a]
  (map #(double (/ % 255)) [r g b a]))
;(clamp-color 163 77 29 255)

;(println (clamp-color 63 55 16 255))
;(println (clamp-color 172 158 91 255))
;(println (clamp-color 91 169 172 255))