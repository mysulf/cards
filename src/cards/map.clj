(ns cards.map)


;; the chance after a successfull encounter to drop an enhancement card.
;; valid for all encounters.
;; this probability is further modified by the difference in level of the heroes
;; and the encounter, so that low level encounters can't be farmed by high level heroes.

;; alternately, have various tiers of upgrade cards, the lower tiers can upgrade
;; the heroes to a lower cap etc.
(def HERO-ENHANCEMENT-PROBABILITY 0.2)


(def TIER-PROBABILITIES [0.5 0.2 0.1 0.01 0.001])

(def encounters {
  1 {:name "Lorian Woods"
   :id 1
   :level 1

   ;; game mode determines how cards are drawn, how the encounter progresses,
   ;; how equipment and heroes are unlocked etc.
   :game-mode :standard

   ;; id's pointing to encounter sets
   :encounter [1 2 3]

   ;; the rewards refer to item and hero id's
   :first-time-rewards [{:hero 5}
                        {:item 32}
                        {:hero-enhancement :strength}]

   ;; guaranteed 1 reward from tier 1
   ;; subsequent tiers are rolled according to the tier probabilities
   :rewards [{:item 31 :tier 1} {:item 2 :tier 1} {:hero-enhancement :random} {:item 45 :tier 2}]

   :unlocked-by []
   :artifacts-required []
   :unlocks [2]
   }

  2 {:name "Lorian Plains"
   :id 2
   :level 1

   ;; game mode determines how cards are drawn, how the encounter progresses,
   ;; how equipment and heroes are unlocked etc.
   :game-mode :standard

   ;; id's pointing to encounter sets
   :encounter [1 2 3]

   ;; the rewards refer to item and hero id's
   :first-time-rewards [{:hero 5}
                        {:item 32}
                        {:hero-enhancement :strength}]

   ;; guaranteed 1 reward from tier 1
   ;; subsequent tiers are rolled according to the tier probabilities
   :rewards [{:item 31 :tier 1} {:item 2 :tier 1} {:hero-enhancement :random} {:item 45 :tier 2}]

   :unlocked-by [1]
   :artifacts-required []
   :unlocks [3 4]
   }
  })

(def profiles [
                ;; profile
                {
                  :name "Player151242"

                  ;; instantiated heroes from template. these are stored with applied enhancements etc, thus changed from
                  ;; the templates.
                  :heroes {
                            1 {"" ""}
                            2 {"" ""}
                            3 {"" ""}
                            }

                  ;; all items in possession. id's referencing item templates. these will not change (unlike heroes),
                  ;; so id's are enough.
                  :items [2 4 1 1 1 2 5 3 2]

                  ;; items selected for the limited inventory
                  :inventory [1 1 2 4]

                  ;; currently selected heroes that will be active in the next encounter
                  :active-party [1 2 3]

                  ;; completed encounter id's. used when determining unlocked map nodes etc.
                  :completed-encounters [1 2 3]

                  ;; some fun stats
                  :various-stats {:killed-monsters 212 :items-picked-up 22 :successful-encounters 5 :failed-encounters 93}
                  }
                ])


;; encumberance: penalty to skill checks (chance to successfully perform skill).
;; maybe penalty to AC and Attack.
;;
;; none: 100%
;; light: ~80% (-2 ac/atk)
;; medium: ~50% (-4 ac/atk)
;; heavy: ~20% (-6 ac/atk)

;; MAIN STATS
;; strength: reduce encumberment penalty
;; dexterity: improves weapon attack roll
;; agility: improves physical resist. improves initiative?
;; vitality: determines bonus hp. starting vit determines hp/level,
;;  current vit gives a flat bonus (maybe some resists?).
;; wisdom: improves magical attack rolls (and magic resists?).


;; skill-levels/bonuses are derived from the main stats.
;; skills have a governing attribute