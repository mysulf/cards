

(def heroes
  {1 {
     :type :hero
     :name "Asheel"
     :dmg-dice-num 1
     :dmg-dice-sides 3
     :crit-multiplier 2
     :crit 20
     :damage-type :physical
     :attack 40
     :hp 90
     :max-hp 90
     :resistances {:physical 120 :magic 0 :fire 0 :poison 0 :thorns 0}
     :bonus-dmg 10
     :id 1
     :initiative 5
     :skills [:disarm-traps :backstab :puncturing-strike]
     }
   2 {
     :type :hero
     :name "Sneeko"
     :dmg-dice-num 1
     :dmg-dice-sides 3
     :crit-multiplier 2
     :crit 20
     :damage-type :physical
     :attack 20
     :hp 7
     :max-hp 7
     :resistances {:physical 13 :magic 0 :fire 0 :poison 0 :thorns 0}
     :bonus-dmg 0
     :id 2
     :initiative 9
     :skills [:berserk :cleave :stunning-blow]
     }
   3 {
     :type :hero
     :name "Sigifijant"
     :dmg-dice-num 1
     :dmg-dice-sides 3
     :crit-multiplier 2
     :crit 20
     :damage-type :physical
     :attack 1
     :hp 10
     :max-hp 10
     :resistances {:physical 16 :magic 0 :fire 0 :poison 0 :thorns 0}
     ;        :ac 16
     :bonus-dmg 0
     :id 3
     :initiative 2
     :skills [:poison-dagger :cure-wounds :healing-ward]
     }
   }
  )
